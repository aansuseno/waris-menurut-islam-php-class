<?php 

/**
 * 
 */
class Waris
{

	private $hartaSiapBagi = 0;
	private $jenisKelaminPewaris = ''; // L atau P
	private $istri = 0;
	private $suami = false;
	private $ibu = false;
	private $ayah = false;
	private $aL = 0; // anak laki-laki
	private $aP = 0; // anak perempuan
	private $cL = 0; // cucu laki-laki dari anak laki-laki
	private $cP = 0; // cucu perempuan dari anak laki-laki
	private $kakek = false; // kakek dari ayah
	private $nenekDariIbu = 0;
	private $nenekDariAyah = 0;
	private $saudaraKandungLaki = 0;
	private $saudaraKandungPerempuan = 0;
	private $saudaraSeAyahLaki = 0;
	private $saudaraSeAyahPerempuan = 0;
	private $saudaraSeIbuLaki = 0;
	private $saudaraSeIbuPerempuan = 0;
	private $anakLakiDariSaudaraLakiKandung = 0;
	private $anakLakiDariSaudaraLakiSeAyah = 0;
	private $pamanKandung = 0; // dari ayah
	private $pamanSeKakek = 0; // dari ayah
	private $anakLakiPamanKandung = 0; // dari ayah
	private $anakLakiPamanSeKakek = 0; // dari ayah

	function __construct($inputHartaSiapBagi = -5, $jk = 'a')
	{
		$jk_baru = strtoupper($jk);
		if (!is_numeric($inputHartaSiapBagi) || $inputHartaSiapBagi < 0) {
			$this->tampilError('Parameter pertama masukkan harta siap bagi dalam bentuk numerik(bukan string) dan harus lebih besar dari 0.');
		} else if($jk_baru != 'P' && $jk_baru != 'L') {
			$this->tampilError('Parameter kedua(wajib diisi) berupa jenis kelamin pewaris. Harus berupa L atau P.');
		}

		$this->hartaSiapBagi = $inputHartaSiapBagi;
		$this->jenisKelaminPewaris = $jk_baru;
	}

	public function tampilError($pesan, $fungsi = "")
	{
		echo "error: ".(($fungsi!== '') ? "di function: <b>$fungsi.</b>" : "")." $pesan";
		exit();
	}

	public function setIstri($jumlah = 0)
	{
		if ($this->jenisKelaminPewaris !== 'L' || $jumlah > 4) {
			$this->tampilError('Operasi gagal karena Muwarrits (Yang meninggal) perempuan. Maksimal empat.', 'setIstri');
		}

		$this->istri = $jumlah;
	}

	public function setSuami($apaAda = false)
	{
		if ($this->jenisKelaminPewaris !== 'P') {
			$this->tampilError('Operasi gagal karena Muwarrits (Yang meninggal) laki-laki.', 'setSuami');
		}

		$this->suami = $apaAda;
	}

	public function setIbu($apaAda = false)
	{
		if (gettype($apaAda) != 'boolean') {
			$this->tampilError('Type data harus berupa boolean (true atau false). True jika mempunyai ibu', 'setIbu');
		}

		$this->ibu = $apaAda;
	}

	public function setAyah($apaAda = false)
	{
		if (gettype($apaAda) != 'boolean') {
			$this->tampilError('Type data harus berupa boolean (true atau false). True jika mempunyai ayah', 'setAyah');
		}

		$this->ayah = $apaAda;
	}

	public function setAnakLaki($jumlah = 0)
	{
		if ($jumlah < 0 || !is_numeric($jumlah)) {
			$this->tampilError('Jumlah tidak boleh lebih kecil dari 0 dan harus berupa bilangan numerik.', 'setAnakLaki');
		}

		$this->aL = $jumlah;
	}

	public function setAnakPerempuan($jumlah = 0)
	{
		if ($jumlah < 0 || !is_numeric($jumlah)) {
			$this->tampilError('Jumlah tidak boleh lebih kecil dari 0 dan harus berupa bilangan numerik.', 'setAnakPerempuan');
		}

		$this->aP = $jumlah;
	}

	// dari anak laki-laki
	public function setCucuLaki($jumlah = 0)
	{
		if ($jumlah < 0 || !is_numeric($jumlah)) {
			$this->tampilError('Jumlah tidak boleh lebih kecil dari 0 dan harus berupa bilangan numerik.', 'setCucuLaki');
		} else if ($this->aL > 0 && $jumlah > 0) {
			$this->tampilError('Cucu laki-laki kosongin saja karena sudah ada anak laki-laki.', 'setCucuLaki');
		}

		$this->cL = $jumlah;
	}

	// dari anak laki-laki
	public function setCucuPerempuan($jumlah = 0)
	{
		if ($jumlah < 0 || !is_numeric($jumlah)) {
			$this->tampilError('Jumlah tidak boleh lebih kecil dari 0 dan harus berupa bilangan numerik.', 'setCucuPerempuan');
		} else if ($this->aL > 0 && $jumlah > 0) {
			$this->tampilError('Cucu perempuan kosongin saja karena sudah ada anak laki-laki.', 'setCucuPerempuan');
		} else if ($this->aP >= 2 && $jumlah > 0) {
			$this->tampilError('Cucu perempuan kosongin saja karena sudah ada '.$this->aP.' anak perempuan.', 'setCucuPerempuan');
		}

		$this->cP = $jumlah;
	}

	public function setKakek($apaAda = false)
	{
		if (gettype($apaAda) != 'boolean') {
			$this->tampilError('Input harus berupa boolean (true/false).', 'setKakek');
		} else if ($this->ayah > 0 && $this->ibu > 0 && $jumlah > 0) {
			$this->tampilError('Kakek kosongin saja karena sudah ada ayah dan ibu.', 'setKakek');
		}

		$this->kakek = $apaAda;
	}

	public function setNenekDariIbu($jumlah = 0)
	{
		if ($jumlah < 0 || !is_numeric($jumlah)) {
			$this->tampilError('Jumlah tidak boleh lebih kecil dari 0 dan harus berupa bilangan numerik.', 'setNenekDariIbu');
		} else if ($this->ayah > 0 && $this->ibu > 0 && $jumlah > 0) {
			$this->tampilError('nenek kosongin saja karena sudah ada ayah dan ibu.', 'setNenekDariIbu');
		} else if ($jumlah > 4) {
			$this->tampilError('Maksimal nenek 4.', 'setNenekDariIbu');
		}

		$this->nenekDariIbu = $jumlah;
	}

	public function setNenekDariAyah($jumlah = 0)
	{
		if ($jumlah < 0 || !is_numeric($jumlah)) {
			$this->tampilError('Jumlah tidak boleh lebih kecil dari 0 dan harus berupa bilangan numerik.', 'setNenekDariAyah');
		} else if ($this->ayah > 0 && $this->ibu > 0 && $jumlah > 0) {
			$this->tampilError('nenek kosongin saja karena sudah ada ayah dan ibu.', 'setNenekDariAyah');
		} else if ($jumlah > 4) {
			$this->tampilError('Maksimal nenek 4.', 'setNenekDariAyah');
		}

		$this->nenekDariAyah = $jumlah;
	}

	public function setSaudaraKandungLaki($jumlah = 0)
	{
		if ($jumlah < 0 || !is_numeric($jumlah)) {
			$this->tampilError('Jumlah tidak boleh lebih kecil dari 0 dan harus berupa bilangan numerik.', 'setSaudaraKandungLaki');
		}

		$this->saudaraKandungLaki = $jumlah;
	}

	public function setSaudaraKandungPerempuan($jumlah = 0)
	{
		if ($jumlah < 0 || !is_numeric($jumlah)) {
			$this->tampilError('Jumlah tidak boleh lebih kecil dari 0 dan harus berupa bilangan numerik.', 'setSaudaraKandungLaki');
		}

		$this->saudaraKandungPerempuan = $jumlah;
	}

	public function setAnakLakiDariSaudaraLakiKandung($jumlah = 0)
	{
		if ($jumlah < 0 || !is_numeric($jumlah)) {
			$this->tampilError('Jumlah tidak boleh lebih kecil dari 0 dan harus berupa bilangan numerik.', 'setSaudaraKandungLaki');
		}

		$this->anakLakiDariSaudaraLakiKandung = $jumlah;
	}

	public function setAnakLakiDariSaudaraLakiSeAyah($jumlah = 0)
	{
		if ($jumlah < 0 || !is_numeric($jumlah)) {
			$this->tampilError('Jumlah tidak boleh lebih kecil dari 0 dan harus berupa bilangan numerik.', 'setAnakLakiDariSaudaraLakiSeAyah');
		} else if($this->anakLakiDariSaudaraLakiKandung > 0) {
			$this->tampilError('Tidak perlu diisi karena sudah terhalang anak laki-laki dari saudara laki-laki kandung', 'setAnakLakiDariSaudaraLakiSeAyah');
		}

		$this->anakLakiDariSaudaraLakiSeAyah = $jumlah;
	}

	public function setPamanKandung($jumlah=0)
	{
		if ($jumlah < 0 || !is_numeric($jumlah)) {
			$this->tampilError('Jumlah tidak boleh lebih kecil dari 0 dan harus berupa bilangan numerik.', 'setPamanKandung');
		}

		$this->pamanKandung = $jumlah;
	}

	public function setPamanSeKakek($jumlah=0)
	{
		if ($jumlah < 0 || !is_numeric($jumlah)) {
			$this->tampilError('Jumlah tidak boleh lebih kecil dari 0 dan harus berupa bilangan numerik.', 'setPamanKandung');
		} else if($this->pamanKandung > 0) {
			$this->tampilError('Lewati saja karena sudah ada paman kandung.', 'setPamanKandung');
		}

		$this->pamanSeKakek = $jumlah;
	}

	public function setAnakLakiPamanKandung($jumlah=0)
	{
		if ($jumlah < 0 || !is_numeric($jumlah)) {
			$this->tampilError('Jumlah tidak boleh lebih kecil dari 0 dan harus berupa bilangan numerik.', 'setAnakLakiPamanKandung');
		}

		$this->anakLakiPamanKandung = $jumlah;
	}

	public function setAnakLakiPamanSeKakek($jumlah=0)
	{
		if ($jumlah < 0 || !is_numeric($jumlah)) {
			$this->tampilError('Jumlah tidak boleh lebih kecil dari 0 dan harus berupa bilangan numerik.', 'setAnakLakiPamanSeKakek');
		}

		$this->anakLakiPamanSeKakek = $jumlah;
	}

	// istri, ibu, ayah, anak laki, anak perempuan, nenek ibu, nenek ayah, kakek, cucu laki

	private function kemungkinan1() // istri, ibu, ayah, anak laki, anak perempuan
	{
		$hasil_istri = ($this->hartaSiapBagi/8)/$this->istri;
		$hasil_ibu = $this->hartaSiapBagi/6;
		$hasil_ayah = $this->hartaSiapBagi/6;
		$sisa = $this->hartaSiapBagi-($this->hartaSiapBagi/6+$this->hartaSiapBagi/6+$this->hartaSiapBagi/8);
		$anak_anak = $sisa/($this->aP + (2*$this->aL));
		$hasil_aP = $anak_anak;
		$hasil_aL = ($anak_anak*2);
		$hasil = [
			[ 
				'siapa'			=> 'Istri',
				'perorang' 	=> round($hasil_istri),
				'jumlah'		=> $this->istri,
			],
			[ 
				'siapa'			=> 'Ibu',
				'perorang' 	=> round($hasil_ibu),
				'jumlah'		=> 1,
			],
			[
				'siapa'			=> 'Ayah',
				'perorang' 	=> round($hasil_ayah),
				'jumlah'		=> 1,
			],
			[
				'siapa'			=> 'Anak Laki-laki',
				'perorang' 	=> round($hasil_aL),
				'jumlah'		=> $this->aL,
			],
			[
				'siapa'			=> 'Anak Perempuan',
				'perorang' 	=> round($hasil_aP),
				'jumlah'		=> $this->aP,
			]
		];

		return $hasil;
	}

	private function kemungkinan2($kondisi = '') // istri, ayah, anak laki, anak perempuan, !nenek ibu, !nenek ayah
	{
		$hasil_istri = ($this->hartaSiapBagi/8)/$this->istri;
		$hasil_ayah = $this->hartaSiapBagi/6;
		$hasil_nenek = 0;
		if ($kondisi != 'tanpa nenek') {
			$hasil_nenek = ($this->hartaSiapBagi/6) / ($this->nenekDariAyah+$this->nenekDariIbu);
			$sisa = $this->hartaSiapBagi-($this->hartaSiapBagi/6+$this->hartaSiapBagi/6+$this->hartaSiapBagi/8);
		} else {
			$sisa = $this->hartaSiapBagi-($this->hartaSiapBagi/6+$this->hartaSiapBagi/8);
		}

		$anak_anak = $sisa/($this->aP + (2*$this->aL));
		$hasil_aP = $anak_anak;
		$hasil_aL = ($anak_anak*2);
		$hasil = [
			[
				'siapa'			=> 'Istri',
				'perorang' 	=> round($hasil_istri),
				'jumlah'		=> $this->istri,
			],
			[
				'siapa'			=> 'Ayah',
				'perorang' 	=> round($hasil_ayah),
				'jumlah'		=> 1,
			],
			[
				'siapa'			=> 'Nenek Dari Ibu',
				'perorang' 	=> round($hasil_nenek),
				'jumlah'		=> $this->nenekDariIbu,
			],
			[
				'siapa'			=> 'Nenek Dari Ayah',
				'perorang' 	=> round($hasil_nenek),
				'jumlah'		=> $this->nenekDariAyah,
			],
			[
				'siapa'			=> 'Anak Laki-laki',
				'perorang' 	=> round($hasil_aL),
				'jumlah'		=> $this->aL,
			],
			[
				'siapa'			=> 'Anak Perempuan',
				'perorang' 	=> round($hasil_aP),
				'jumlah'		=> $this->aP,
			]
		];

		if ($kondisi === 'tanpa nenek dari ibu') {
			$hasil_baru = [];
			foreach ($hasil as $i) {
				if (!($i['siapa'] == 'Nenek Dari Ibu')) {
					array_push($hasil_baru, $i);
				}
			}
			return $hasil_baru;
		} else if($kondisi === 'tanpa nenek dari ayah') {
			$hasil_baru = [];
			foreach ($hasil as $i) {
				if (!($i['siapa'] == 'Nenek Dari Ayah')) {
					array_push($hasil_baru, $i);
				}
			}
			return $hasil_baru;
		} else if($kondisi === 'tanpa nenek') {
			$hasil_baru = [];
			foreach ($hasil as $i) {
				if (!($i['siapa'] == 'Nenek Dari Ayah') && !($i['siapa'] == 'Nenek Dari Ibu')) {
					array_push($hasil_baru, $i);
				}
			}
			return $hasil_baru;
		}

		return $hasil;
	}

	private function kemungkinan3($kondisi = '') // istri, ibu, anak laki, anak perempuan, !kakek
	{
		$hasil_istri = ($this->hartaSiapBagi/8)/$this->istri;
		$hasil_ibu = $this->hartaSiapBagi/6;
		$hasil_kakek = 0;
		if ($kondisi != 'tanpa kakek') {
			$hasil_kakek = $this->hartaSiapBagi/6;
			$sisa = $this->hartaSiapBagi-($this->hartaSiapBagi/6+$this->hartaSiapBagi/6+$this->hartaSiapBagi/8);
		} else {
			$sisa = $this->hartaSiapBagi-($this->hartaSiapBagi/6+$this->hartaSiapBagi/8);
		}

		$anak_anak = $sisa/($this->aP + (2*$this->aL));
		$hasil_aP = $anak_anak;
		$hasil_aL = ($anak_anak*2);
		$hasil = [
			[
				'siapa'			=> 'Istri',
				'perorang' 	=> round($hasil_istri),
				'jumlah'		=> $this->istri,
			],
			[
				'siapa'			=> 'Ibu',
				'perorang' 	=> round($hasil_ibu),
				'jumlah'		=> 1,
			],
			[
				'siapa'			=> 'Kakek',
				'perorang' 	=> round($hasil_kakek),
				'jumlah'		=> 1,
			],
			[
				'siapa'			=> 'Anak Laki-laki',
				'perorang' 	=> round($hasil_aL),
				'jumlah'		=> $this->aL,
			],
			[
				'siapa'			=> 'Anak Perempuan',
				'perorang' 	=> round($hasil_aP),
				'jumlah'		=> $this->aP,
			]
		];

		if ($kondisi === 'tanpa kakek') {
			$hasil_baru = [];
			foreach ($hasil as $i) {
				if ($i['siapa'] != 'Kakek') {
					array_push($hasil_baru, $i);
				}
			}
			return $hasil_baru;
		}

		return $hasil;
	}

	private function kemungkinan4($kondisi='') // istri, anak laki, anak perempuan kakek, !nenek ibu, !nenek ayah
	{
		$hasil_istri = ($this->hartaSiapBagi/8)/$this->istri;
		$hasil_kakek = $this->hartaSiapBagi/6;
		$hasil_nenek = 0;
		$sisa = $this->hartaSiapBagi-($this->hartaSiapBagi/6+$this->hartaSiapBagi/6+$this->hartaSiapBagi/8);

		if ($kondisi == 'tanpa nenek') {
			$sisa = $this->hartaSiapBagi-($this->hartaSiapBagi/6+$this->hartaSiapBagi/8);
		} else {
			$hasil_nenek = $hasil_kakek/($this->nenekDariIbu+$this->nenekDariAyah);
		}

		$anak_anak = $sisa/($this->aP + (2*$this->aL));
		$hasil_aP = $anak_anak;
		$hasil_aL = ($anak_anak*2);
		$hasil = [
			[
				'siapa'			=> 'Istri',
				'perorang' 	=> round($hasil_istri),
				'jumlah'		=> $this->istri,
			],
			[
				'siapa'			=> 'Kakek',
				'perorang' 	=> round($hasil_kakek),
				'jumlah'		=> 1,
			],
			[
				'siapa'			=> 'Nenek Dari Ayah',
				'perorang' 	=> round($hasil_nenek),
				'jumlah'		=> 1,
			],
			[
				'siapa'			=> 'Nenek Dari Ibu',
				'perorang' 	=> round($hasil_nenek),
				'jumlah'		=> 1,
			],
			[
				'siapa'			=> 'Anak Laki-laki',
				'perorang' 	=> round($hasil_aL),
				'jumlah'		=> $this->aL,
			],
			[
				'siapa'			=> 'Anak Perempuan',
				'perorang' 	=> round($hasil_aP),
				'jumlah'		=> $this->aP,
			]
		];

		if ($kondisi === 'tanpa nenek-ibu') {
			$hasil_baru = [];
			foreach ($hasil as $i) {
				if ($i['siapa'] != 'Nenek Dari Ibu') {
					array_push($hasil_baru, $i);
				}
			}
			return $hasil_baru;
		} else if ($kondisi === 'tanpa nenek-ayah') {
			$hasil_baru = [];
			foreach ($hasil as $i) {
				if ($i['siapa'] != 'Nenek Dari Ayah') {
					array_push($hasil_baru, $i);
				}
			}
			return $hasil_baru;
		} else if ($kondisi === 'tanpa nenek') {
			$hasil_baru = [];
			foreach ($hasil as $i) {
				if ($i['siapa'] != 'Nenek Dari Ayah' && $i['siapa'] != 'Nenek Dari Ibu') {
					array_push($hasil_baru, $i);
				}
			}
			return $hasil_baru;
		}

		return $hasil;
	}

	private function kemungkinan5($kondisi='') // istri, anak laki, anak perempuan, !nenek ibu, !nenek ayah
	{
		$hasil_istri = ($this->hartaSiapBagi/8)/$this->istri;
		$hasil_nenek = 0;
		$sisa = $this->hartaSiapBagi-($this->hartaSiapBagi/6+$this->hartaSiapBagi/8);

		if ($kondisi == 'tanpa nenek') {
			$sisa = $this->hartaSiapBagi-($this->hartaSiapBagi/8);
		} else {
			$hasil_nenek = ($this->hartaSiapBagi/6)/($this->nenekDariIbu+$this->nenekDariAyah);
		}

		$anak_anak = $sisa/($this->aP + (2*$this->aL));
		$hasil_aP = $anak_anak;
		$hasil_aL = ($anak_anak*2);
		$hasil = [
			[
				'siapa'			=> 'Istri',
				'perorang' 	=> round($hasil_istri),
				'jumlah'		=> $this->istri,
			],
			[
				'siapa'			=> 'Nenek Dari Ayah',
				'perorang' 	=> round($hasil_nenek),
				'jumlah'		=> 1,
			],
			[
				'siapa'			=> 'Nenek Dari Ibu',
				'perorang' 	=> round($hasil_nenek),
				'jumlah'		=> 1,
			],
			[
				'siapa'			=> 'Anak Laki-laki',
				'perorang' 	=> round($hasil_aL),
				'jumlah'		=> $this->aL,
			],
			[
				'siapa'			=> 'Anak Perempuan',
				'perorang' 	=> round($hasil_aP),
				'jumlah'		=> $this->aP,
			]
		];

		if ($kondisi === 'tanpa nenek-ibu') {
			$hasil_baru = [];
			foreach ($hasil as $i) {
				if ($i['siapa'] != 'Nenek Dari Ibu') {
					array_push($hasil_baru, $i);
				}
			}
			return $hasil_baru;
		} else if ($kondisi === 'tanpa nenek-ayah') {
			$hasil_baru = [];
			foreach ($hasil as $i) {
				if ($i['siapa'] != 'Nenek Dari Ayah') {
					array_push($hasil_baru, $i);
				}
			}
			return $hasil_baru;
		} else if ($kondisi === 'tanpa nenek') {
			$hasil_baru = [];
			foreach ($hasil as $i) {
				if ($i['siapa'] != 'Nenek Dari Ayah' && $i['siapa'] != 'Nenek Dari Ibu') {
					array_push($hasil_baru, $i);
				}
			}
			return $hasil_baru;
		}

		return $hasil;
	}

	private function kemungkinan6($kondisi='') // istri, ibu, ayah, anak perempuan, !cucu laki
	{
		// aul 27
		$aul = $this->hartaSiapBagi/27;
		$hasil_istri = $aul * 3 / $this->istri;
		$hasil_ibu = $aul * 4;
		$hasil_ayah = $aul * 4;
		$hasil_aP = $aul * 16 / $this->aP;

		$hasil_cL = 0;
		$hasil = [
			[
				'siapa'			=> 'Istri',
				'perorang' 	=> round($hasil_istri),
				'jumlah'		=> $this->istri,
			],
			[
				'siapa'			=> 'Ibu',
				'perorang' 	=> round($hasil_ibu),
				'jumlah'		=> 1,
			],
			[
				'siapa'			=> 'Ayah',
				'perorang' 	=> round($hasil_ayah),
				'jumlah'		=> 1,
			],
			[
				'siapa'			=> 'Anak Perempuan',
				'perorang' 	=> round($hasil_aP),
				'jumlah'		=> $this->aP,
			],
			[
				'siapa'			=> 'Cucu Laki-laki',
				'perorang' 	=> round($hasil_cL),
				'jumlah'		=> $this->cL,
			],
		];

		if ($kondisi == 'tanpa cucu-laki') {
			$hasil_baru = [];
			foreach ($hasil as $i) {
				if ($i['siapa'] != 'Cucu Laki-laki') {
					array_push($hasil_baru, $i);
				}
			}
			return $hasil_baru;
		}

		return $hasil;
	}

	private function kemungkinan7($value='', $arr = []) // istri, ayah, anak perempuan, !nenek ibu, !nenek ayah, !cucu laki
	{
		// aul
		$aul = $this->hartaSiapBagi/27;
		$hasil_istri = $aul * 3 / $this->istri;
		$hasil_ayah = $aul * 4;
		$hasil_aP = $aul * 16 / $this->aP;
		$hasil_nenek = 0;
		if ($this->nenekDariIbu+$this->nenekDariAyah != 0) {
			$hasil_nenek = $aul * 4 / ($this->nenekDariIbu+$this->nenekDariAyah);
		}

		$hasil_cL = 0;
		$sisa = $this->hartaSiapBagi-($aul * 3+$aul * 4+$aul * 16+$hasil_nenek*($this->nenekDariIbu+$this->nenekDariAyah));
		$hasil = [
			[
				'siapa'			=> 'Istri',
				'perorang' 	=> round($hasil_istri),
				'jumlah'		=> $this->istri,
			],
			[
				'siapa'			=> 'Ayah',
				'perorang' 	=> round($hasil_ayah),
				'jumlah'		=> 1,
			],
			[
				'siapa'			=> 'Nenek Dari Ibu',
				'perorang' 	=> round($hasil_nenek),
				'jumlah'		=> $this->nenekDariIbu,
			],
			[
				'siapa'			=> 'Nenek Dari Ayah',
				'perorang' 	=> round($hasil_nenek),
				'jumlah'		=> $this->nenekDariAyah,
			],
			[
				'siapa'			=> 'Anak Perempuan',
				'perorang' 	=> round($hasil_aP),
				'jumlah'		=> $this->aP,
			],
			[
				'siapa'			=> 'Cucu Laki-laki',
				'perorang' 	=> round($hasil_cL),
				'jumlah'		=> $this->cL,
			],
		];
		// print_r($arr);exit();
		if (count($arr) > 0) {
			$hasil_baru = [];
			foreach ($hasil as $j) {
				$oke = false;
				foreach($arr as $i) { 
					if ($j['siapa'] == $i) {
						$oke = true;
					}
				}
				if (!$oke) {
					array_push($hasil_baru, $j);
				}
			}
			if ($sisa>0) {
				array_push($hasil_baru, [
					'siapa' => 'Ayah (asabah)',
					'perorang' => round($sisa),
					'jumlah' => 1,
				]);
			}
			return $hasil_baru;
		}

		if ($value != '' && $value != 'nenek ayah-ibu') {
			$hasil_baru = [];
			foreach ($hasil as $i) {
				if ($i['siapa'] != $value) {
					array_push($hasil_baru, $i);
				}
			}
			return $hasil_baru;
		} else if ($value == 'nenek ayah-ibu') {
			$hasil_baru = [];
			foreach ($hasil as $i) {
				if ($i['siapa'] != 'Nenek Dari Ayah' && $i['siapa'] != 'Nenek Dari Ibu') {
					array_push($hasil_baru, $i);
				}
			}
			return $hasil_baru;
		}
		// echo count($arr);

		return $hasil;
	}

	private function kemungkinan8($value='') // istri, ayah, anak perempuan, !cucu laki
	{
		$aul = $this->hartaSiapBagi/24;
		$hasil_istri = $aul * 3 / $this->istri;
		$hasil_ayah = $aul * 4;
		$hasil_aP = $aul * 16 / $this->aP;

		$hasil_cL = ($this->hartaSiapBagi - ($aul * 3+$aul * 4+$aul * 16)) / $this->cL ;
		$hasil = [
			[
				'siapa'			=> 'Istri',
				'perorang' 	=> round($hasil_istri),
				'jumlah'		=> $this->istri,
			],
			[
				'siapa'			=> 'Ayah',
				'perorang' 	=> round($hasil_ayah),
				'jumlah'		=> 1,
			],
			[
				'siapa'			=> 'Anak Perempuan',
				'perorang' 	=> round($hasil_aP),
				'jumlah'		=> $this->aP,
			],
			[
				'siapa'			=> 'Cucu Laki-laki',
				'perorang' 	=> round($hasil_cL),
				'jumlah'		=> $this->cL,
			],
		];

		return $hasil;
	}

	private function kemungkinan9($value='', $arr = []) // istri, ibu, anak perempuan, kakek, cucu laki
	{
		// aul
		$aul = $this->hartaSiapBagi/27;
		$hasil_istri = $aul * 3 / $this->istri;
		$hasil_ibu = $aul * 4;
		$hasil_kakek = $aul * 4 / $this->kakek;
		$hasil_aP = $aul * 16 / $this->aP;
		$hasil_cL = 0;

		$hasil = [
			[
				'siapa'			=> 'Istri',
				'perorang' 	=> round($hasil_istri),
				'jumlah'		=> $this->istri,
			],
			[
				'siapa'			=> 'Ibu',
				'perorang' 	=> round($hasil_ibu),
				'jumlah'		=> 1,
			],
			[
				'siapa'			=> 'Anak Perempuan',
				'perorang' 	=> round($hasil_aP),
				'jumlah'		=> $this->aP,
			],
			[
				'siapa'			=> 'Kakek',
				'perorang' 	=> round($hasil_kakek),
				'jumlah'		=> $this->kakek,
			],
			[
				'siapa'			=> 'Cucu Laki-laki',
				'perorang' 	=> round($hasil_cL),
				'jumlah'		=> $this->cL,
			],
		];

		if (count($arr) > 0) {
			$hasil_baru = [];
			foreach ($hasil as $j) {
				$oke = false;
				foreach($arr as $i) { 
					if ($j['siapa'] == $i) {
						$oke = true;
					}
				}
				if (!$oke) {
					array_push($hasil_baru, $j);
				}
			}
			return $hasil_baru;
		}

		return $hasil;
	}

	private function kemungkinan10($value='', $arr = []) // istri, ibu, anak perempuan, cucu laki
	{
		// aul
		$aul = $this->hartaSiapBagi/24;
		$hasil_istri = $aul * 3 / $this->istri;
		$hasil_ibu = $aul * 4;
		$hasil_aP = $aul * 16 / $this->aP;
		$sisa = $this->hartaSiapBagi - ($aul * 3 + $aul * 4 + $aul * 16);
		// echo $sisa;
		$hasil_cL = $sisa/$this->cL;

		$hasil = [
			[
				'siapa'			=> 'Istri',
				'perorang' 	=> round($hasil_istri),
				'jumlah'		=> $this->istri,
			],
			[
				'siapa'			=> 'Ibu',
				'perorang' 	=> round($hasil_ibu),
				'jumlah'		=> 1,
			],
			[
				'siapa'			=> 'Anak Perempuan',
				'perorang' 	=> round($hasil_aP),
				'jumlah'		=> $this->aP,
			],
			[
				'siapa'			=> 'Cucu Laki-laki',
				'perorang' 	=> round($hasil_cL),
				'jumlah'		=> $this->cL,
			],
		];

		return $hasil;
	}

	private function kemungkinan11($arr = []) // istri, ibu, anak perempuan, saudara kandung laki, saudara kandung perempuan
	{
		// aul
		$aul = $this->hartaSiapBagi/24;
		$hasil_istri = $aul * 3 / $this->istri;
		$hasil_ibu = $aul * 4;
		$hasil_aP = $aul * 16 / $this->aP;
		$sisa = $this->hartaSiapBagi - ($aul * 3 + $aul * 4 + $aul * 16);
		$hasil_sdr = $sisa / ($this->saudaraKandungLaki*2+$this->saudaraKandungPerempuan);

		$hasil = [
			[
				'siapa'			=> 'Istri',
				'perorang' 	=> round($hasil_istri),
				'jumlah'		=> $this->istri,
			],
			[
				'siapa'			=> 'Ibu',
				'perorang' 	=> round($hasil_ibu),
				'jumlah'		=> 1,
			],
			[
				'siapa'			=> 'Anak Perempuan',
				'perorang' 	=> round($hasil_aP),
				'jumlah'		=> $this->aP,
			],
			[
				'siapa'			=> 'Saudara Kandung Laki-laki',
				'perorang' 	=> round($hasil_sdr*2),
				'jumlah'		=> $this->saudaraKandungLaki,
			],
			[
				'siapa'			=> 'Saudara Kandung Perempuan',
				'perorang' 	=> round($hasil_sdr),
				'jumlah'		=> $this->saudaraKandungPerempuan,
			],
		];

		if (count($arr) > 0) {
			$hasil_baru = [];
			foreach ($hasil as $j) {
				$oke = false;
				foreach($arr as $i) { 
					if ($j['siapa'] == $i) {
						$oke = true;
					}
				}
				if (!$oke) {
					array_push($hasil_baru, $j);
				}
			}
			return $hasil_baru;
		}

		return $hasil;
	}

	private function kemungkinan12($value='',$arr = []) // istri, ibu, anak perempuan, saudara kandung perempuan, anak L sdr kandung
	{
		// aul
		$aul = $this->hartaSiapBagi/24;
		$hasil_istri = $aul * 3 / $this->istri;
		$hasil_ibu = $aul * 4;
		$hasil_aP = $aul * 16 / $this->aP;
		$sisa = $this->hartaSiapBagi - ($aul * 3 + $aul * 4 + $aul * 16);
		$hasil_sdr = $sisa / ($this->saudaraKandungPerempuan+$this->anakLakiDariSaudaraLakiKandung);
		if ($value == 'seayah') {
			$hasil_sdr = $sisa / ($this->saudaraKandungPerempuan+$this->anakLakiDariSaudaraLakiSeAyah);
		} else if ($value == 'pamanKandung') {
			$hasil_sdr = $sisa / ($this->saudaraKandungPerempuan+$this->pamanKandung);
		} else if ($value == 'pamanSeKakek') {
			$hasil_sdr = $sisa / ($this->saudaraKandungPerempuan+$this->pamanSeKakek);
		} else if ($value == 'anakLakiPamanKandung') {
			$hasil_sdr = $sisa / ($this->saudaraKandungPerempuan+$this->anakLakiPamanKandung);
		} else if ($value == 'anakLakiPamanSeKakek') {
			$hasil_sdr = $sisa / ($this->saudaraKandungPerempuan+$this->anakLakiPamanSeKakek);
		}

		$hasil = [
			[
				'siapa'			=> 'Istri',
				'perorang' 	=> round($hasil_istri),
				'jumlah'		=> $this->istri,
			],
			[
				'siapa'			=> 'Ibu',
				'perorang' 	=> round($hasil_ibu),
				'jumlah'		=> 1,
			],
			[
				'siapa'			=> 'Anak Perempuan',
				'perorang' 	=> round($hasil_aP),
				'jumlah'		=> $this->aP,
			],
			[
				'siapa'			=> 'Saudara Kandung Perempuan',
				'perorang' 	=> round($hasil_sdr),
				'jumlah'		=> $this->saudaraKandungPerempuan,
			],
			[
				'siapa'			=> 'Anak Laki-laki saudara Laki-laki Sekandung',
				'perorang' 	=> round($hasil_sdr),
				'jumlah'		=> $this->anakLakiDariSaudaraLakiKandung,
			],
		];

		if ($value == 'seayah') {
			array_pop($hasil);
			array_push($hasil, [
				'siapa' => 'Saudara Laki-laki saudara Laki-laki SeAyah',
				'perorang' 	=> round($hasil_sdr),
				'jumlah'		=> $this->anakLakiDariSaudaraLakiSeAyah,
			]);
		} else if ($value == 'pamanKandung') {
			array_pop($hasil);
			array_push($hasil, [
				'siapa' => 'Paman Kandung',
				'perorang' 	=> round($hasil_sdr),
				'jumlah'		=> $this->pamanKandung,
			]);
		} else if ($value == 'pamanSeKakek') {
			array_pop($hasil);
			array_push($hasil, [
				'siapa' => 'Paman Satu kakek',
				'perorang' 	=> round($hasil_sdr),
				'jumlah'		=> $this->pamanSeKakek,
			]);
		} else if ($value == 'anakLakiPamanKandung') {
			array_pop($hasil);
			array_push($hasil, [
				'siapa' => 'Anak Laki-laki Dari Paman Kandung',
				'perorang' 	=> round($hasil_sdr),
				'jumlah'		=> $this->anakLakiPamanKandung,
			]);
		} else if ($value == 'anakLakiPamanSeKakek') {
			array_pop($hasil);
			array_push($hasil, [
				'siapa' => 'Anak Laki-laki Dari Paman Satu kakek',
				'perorang' 	=> round($hasil_sdr),
				'jumlah'		=> $this->anakLakiPamanSeKakek,
			]);
		}

		return $hasil;
	}

	private function kemungkinan14($value='',$arr = []) // istri, ibu, anak perempuan, saudara kandung perempuan, anak L sdr kandung
	{
		// raad
		$aul = $this->hartaSiapBagi/24;
		$hasil_istri = $aul * 3 / $this->istri;
		$sisa = $this->hartaSiapBagi - ($aul * 3);
		$hasil_ibu = $sisa / 6;
		$hasil_aP = $sisa * 2 / 3 / $this->aP;

		$hasil = [
			[
				'siapa'			=> 'Istri',
				'perorang' 	=> round($hasil_istri),
				'jumlah'		=> $this->istri,
			],
			[
				'siapa'			=> 'Ibu',
				'perorang' 	=> round($hasil_ibu),
				'jumlah'		=> 1,
			],
			[
				'siapa'			=> 'Anak Perempuan',
				'perorang' 	=> round($hasil_aP),
				'jumlah'		=> $this->aP,
			],
		];

		return $hasil;
	}

	public function hitung()
	{
		if ($this->jenisKelaminPewaris == 'L') {
			if ($this->istri > 0) {
				if ($this->aL > 0) {
					if ($this->aP >= 0) {
						if ($this->ayah) {
							if ($this->ibu) {
								return $this->kemungkinan1();
							} else {
								if ($this->nenekDariAyah > 0) {
									if ($this->nenekDariIbu > 0) {
										return $this->kemungkinan2();
									} else {
										return $this->kemungkinan2('tanpa nenek dari ibu');
									}
								} else {
									if ($this->nenekDariIbu > 0) {
										return $this->kemungkinan2('tanpa nenek dari ayah');
									} else {
										return $this->kemungkinan2('tanpa nenek');
									}
								}
							}
						} else {
							if ($this->ibu) {
								if ($this->kakek) {
									return $this->kemungkinan3();
								} else {
									return $this->kemungkinan3('tanpa kakek');
								}
							} else {
								if ($this->kakek) {
									if ($this->nenekDariAyah > 0) {
										if ($this->nenekDariIbu > 0) {
											return $this->kemungkinan4();
										} else {
											return $this->kemungkinan4('tanpa nenek-ibu');
										}
									} else {
										if ($this->nenekDariIbu > 0) {
											return $this->kemungkinan4('tanpa nenek-ayah');
										} else {
											return $this->kemungkinan4('tanpa nenek');
										}
									}
								} else {
									if ($this->nenekDariAyah > 0) {
										if ($this->nenekDariIbu > 0) {
											return $this->kemungkinan5();
										} else {
											return $this->kemungkinan5('tanpa nenek-ibu');
										}
									} else {
										if ($this->nenekDariIbu > 0) {
											return $this->kemungkinan5('tanpa nenek-ayah');
										} else {
											return $this->kemungkinan5('tanpa nenek');
										}
									}
								}
							}
						}
					}
				} else {
					if($this->aP >= 2) {
						if ($this->ayah) {
							if ($this->ibu) {
								if ($this->cL > 0) {
									return $this->kemungkinan6();
								} else {
									return $this->kemungkinan6('tanpa cucu-laki');
								}
							} else {
								if ($this->cL > 0) {
									if ($this->nenekDariAyah > 0) {
										if ($this->nenekDariIbu > 0) {
											return $this->kemungkinan7();
										} else {
											return $this->kemungkinan7('Nenek Dari Ibu');
										}
									} else {
										if ($this->nenekDariIbu > 0) {
											return $this->kemungkinan7('Nenek Dari Ayah');
										} else {
											return $this->kemungkinan8();
										}
									}
								} else {
									if ($this->nenekDariAyah > 0) {
										if ($this->nenekDariIbu > 0) {
											return $this->kemungkinan7('arr', ['Cucu Laki-laki']);
										} else {
											return $this->kemungkinan7('arr', ['Cucu Laki-laki', 'Nenek Dari Ibu']);
										}
									} else {
										if ($this->nenekDariIbu > 0) {
											return $this->kemungkinan7('arr', ['Cucu Laki-laki', 'Nenek Dari Ayah']);
										} else {
											return $this->kemungkinan7('arr', ['Cucu Laki-laki', 'Nenek Dari Ibu', 'Nenek Dari Ayah']);
										}
									}
								}
							}
						} else {
							if ($this->ibu) {
								if ($this->cL > 0) {
									if ($this->kakek) {
										return $this->kemungkinan9();
									} else {
										return $this->kemungkinan10();
									}
								} else {
									if ($this->kakek) {
										return $this->kemungkinan9('arr', ['Cucu Laki-laki']);
									} else {
										if ($this->saudaraKandungLaki > 0) {
											if ($this->saudaraKandungPerempuan > 0) {
												return $this->kemungkinan11();
											} else {
												return $this->kemungkinan11(['Saudara Kandung Perempuan']);
											}
										} else {
											if ($this->saudaraKandungPerempuan > 0) {
												if ($this->anakLakiDariSaudaraLakiKandung > 0) {
													return $this->kemungkinan12();
												} else {
													if ($this->anakLakiDariSaudaraLakiSeAyah > 0) {
														return $this->kemungkinan12('seayah');
													} else {
														if ($this->pamanKandung > 0) {
															return $this->kemungkinan12('pamanKandung');
														} else {
															if ($this->pamanSeKakek > 0) {
																return $this->kemungkinan12('pamanSeKakek');
															} else {
																if ($this->anakLakiPamanKandung > 0) {
																	return $this->kemungkinan12('anakLakiPamanKandung');
																} else {
																	if ($this->anakLakiPamanSeKakek > 0) {
																		return $this->kemungkinan12('anakLakiPamanSeKakek');
																	} else {
																		return $this->kemungkinan14();
																	}
																}
															}
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
		return ['error' => 'belum ditentukan'];
	}
}

$tes = new Waris(200000, 'l');
$tes->setIstri(2);
// $tes->setAyah(true);
$tes->setIbu(true);
// $tes->setAnakLaki(2);
$tes->setAnakPerempuan(2);
// $tes->setCucuLaki(2);
// $tes->setKakek(true);
// $tes->setNenekDariAyah(2);
// $tes->setNenekDariIbu(2);
// $tes->setSaudaraKandungLaki(2);
$tes->setSaudaraKandungPerempuan(2);
// $tes->setAnakLakiDariSaudaraLakiKandung(2);
// $tes->setAnakLakiDariSaudaraLakiSeAyah(2);
// $tes->setPamanKandung(2);
// $tes->setPamanSeKakek(2);
// $tes->setAnakLakiPamanKandung(2);
// $tes->setAnakLakiPamanSeKakek(2);

echo "<pre>";
print_r($tes->hitung());
 ?>