<?php 

/**
 * 
 */
class Waris
{
	private $iJthSuami=0;
	private $iJthIstri=0;
	private $iJthBapak=0;
	private $iJthIbu=0;
	private $iJthAnakLaki=0;
	private $iJthAnakPerempuan=0;
	private $iJthCucuLaki=0;
	private $iJthCucuPerempuan=0;
	private $iJthKakek=0;
	private $iJthNenekBapak=0;
	private $iJthNenekIbu=0;
	private $iJthNenekKakek=0;
	private $iJthSaudaraKandung=0;
	private $iJthSaudariKandung=0;
	private $iJthSaudaraSebapak=0;
	private $iJthSaudaraSeibu=0;
	private $iJthSaudariSebapak=0;
	private $iJthSaudariSeibu=0;
	private $iJthPutraSaudaraKandung=0;
	private $iJthPutraSaudaraSebapak=0;
	private $iJthPamanKandung=0;
	private $iJthPamanSebapak=0;
	private $iJthPutraPamanKandung=0;
	private $iJthPutraPamanSebapak=0;
	private $iJthPriaMerdekakan=0;
	private $iJthWanitaMerdekakan=0;
	// private $iHarta = 0;
	private $iSisa = 0;
	private $iTarikah = 0;
	private $iHak1 = 0;
	private $iHak2 = 0;
	private $iHak3 = 0;
	private $iHak4 = 0;
	private $iHarta = 0;
	private $iJumlahBapak = 0;
	private $iJumlahIbu = 0;
	private $iJumlahSuami = 0;
	private $iJumlahIstri = 0;
	private $iJumlahAnakLaki = 0;
	private $iJumlahAnakPerempuan = 0;
	private $iJumlahCucuLaki = 0;
	private $iJumlahCucuPerempuan = 0;
	private $iJumlahKakek = 0;
	private $iJumlahNenekBapak = 0;
	private $iJumlahNenekIbu = 0;
	private $iJumlahNenekKakek = 0;
	private $iJumlahSaudaraKandung = 0;
	private $iJumlahSaudariKandung = 0;
	private $iJumlahSaudaraSebapak = 0;
	private $iJumlahSaudaraSeibu = 0;
	private $iJumlahSaudariSebapak = 0;
	private $iJumlahSaudariSeibu = 0;
	private $iJumlahPutraSaudaraKandung = 0;
	private $iJumlahPutraSaudaraSebapak = 0;
	private $iJumlahPamanKandung = 0;
	private $iJumlahPamanSebapak = 0;
	private $iJumlahPutraPamanKandung = 0;
	private $iJumlahPutraPamanSebapak = 0;
	private $iJumlahPriaMerdekakan = 0;
	private $iJumlahWanitaMerdekakan = 0;
	private $res = [];
	private $gagal = [];
	private $aman = true;

	function __construct($argumen = [
		'harta' => 0,
		'hak1' => 0,
		'hak2' => 0,
		'hak3' => 0,
		'hak4' => 0,
		'bapak' => 0,
		'ibu' => 0,
		'suami' => 0,
		'istri' => 0,
		'putra' => 0,
		'putri' => 0,
		'cucuLk' => 0,
		'cucuPr' => 0,
		'kakek' => 0,
		'nenekB' => 0,
		'nenekI' => 0,
		'nenekK' => 0,
		'saudaraK' => 0,
		'saudariK' => 0,
		'saudaraB' => 0,
		'saudaraI' => 0,
		'saudariB' => 0,
		'saudariI' => 0,
		'putraSK' => 0,
		'putraSB' => 0,
		'pamanK' => 0,
		'pamanB' => 0,
		'putraPK' => 0,
		'putraPB' => 0,
		'pria' => 0,
		'wanita' => 0,
	])
	{
		$this->iTarikah	= (isset($argumen['harta']))?$argumen['harta']:0;
		$this->iHak1 = (isset($argumen['hak1']))?$argumen['hak1']:0;
		$this->iHak2 = (isset($argumen['hak2']))?$argumen['hak2']:0;
		$this->iHak3 = (isset($argumen['hak3']))?$argumen['hak3']:0;
		$this->iHak4 = (isset($argumen['hak4']))?$argumen['hak4']:0;
		$this->iHarta = $this->iTarikah - ($this->iHak1 + $this->iHak2 + $this->iHak3 + $this->iHak4);
		$this->iJumlahBapak = (isset($argumen['bapak']))?$argumen['bapak']:0;
		$this->iJumlahIbu = (isset($argumen['ibu']))?$argumen['ibu']:0;
		$this->iJumlahSuami = (isset($argumen['suami']))?$argumen['suami']:0;
		$this->iJumlahIstri = (isset($argumen['istri']))?$argumen['istri']:0;
		$this->iJumlahAnakLaki = (isset($argumen['putra']))?$argumen['putra']:0;
		$this->iJumlahAnakPerempuan = (isset($argumen['putri']))?$argumen['putri']:0;
		$this->iJumlahCucuLaki = (isset($argumen['cucuLk']))?$argumen['cucuLk']:0;
		$this->iJumlahCucuPerempuan = (isset($argumen['cucuPr']))?$argumen['cucuPr']:0;
		$this->iJumlahKakek = (isset($argumen['kakek']))?$argumen['kakek']:0;
		$this->iJumlahNenekBapak = (isset($argumen['nenekB']))?$argumen['nenekB']:0;
		$this->iJumlahNenekIbu = (isset($argumen['nenekI']))?$argumen['nenekI']:0;
		$this->iJumlahNenekKakek = (isset($argumen['nenekK']))?$argumen['nenekK']:0;
		$this->iJumlahSaudaraKandung = (isset($argumen['saudaraK']))?$argumen['saudaraK']:0;
		$this->iJumlahSaudariKandung = (isset($argumen['saudariK']))?$argumen['saudariK']:0;
		$this->iJumlahSaudaraSebapak = (isset($argumen['saudaraB']))?$argumen['saudaraB']:0;
		$this->iJumlahSaudaraSeibu = (isset($argumen['saudaraI']))?$argumen['saudaraI']:0;
		$this->iJumlahSaudariSebapak = (isset($argumen['saudariB']))?$argumen['saudariB']:0;
		$this->iJumlahSaudariSeibu = (isset($argumen['saudariI']))?$argumen['saudariI']:0;
		$this->iJumlahPutraSaudaraKandung = (isset($argumen['putraSK']))?$argumen['putraSK']:0;
		$this->iJumlahPutraSaudaraSebapak = (isset($argumen['putraSB']))?$argumen['putraSB']:0;
		$this->iJumlahPamanKandung = (isset($argumen['pamanK']))?$argumen['pamanK']:0;
		$this->iJumlahPamanSebapak = (isset($argumen['pamanB']))?$argumen['pamanB']:0;
		$this->iJumlahPutraPamanKandung = (isset($argumen['putraPK']))?$argumen['putraPK']:0;
		$this->iJumlahPutraPamanSebapak = (isset($argumen['putraPB']))?$argumen['putraPB']:0;
		$this->iJumlahPriaMerdekakan = (isset($argumen['pria']))?$argumen['pria']:0;
		$this->iJumlahWanitaMerdekakan = (isset($argumen['wanita']))?$argumen['wanita']:0;
	}

	private function ggl($pesan)
	{
		array_push($this->gagal, $pesan);
	}

	public function getCek()
	{
		return (count($this->gagal) > 0) ? false : true;
	}

	private function cek()
	{
		if ($this->iTarikah <= 0)  {
			$this->ggl('Harta awal harus diisi');
		}
		if($this->iHak4 > $this->iTarikah/3){
			$this->ggl("Wasiat Tidak Boleh lebih dari 1/3 Tarikah. Isi form wasiat lagi");
		}
		if($this->iHarta <= 0){
			$this->ggl("Harta Irst <= 0, Penghitungan Waris tidak bisa dilanjutkan. Ulangi dari awal");
		}
		if ($this->iJumlahBapak > 1) {
			$this->ggl("Bapak hanya boleh ada satu (1)");
		}
		if ($this->iJumlahSuami > 1) {
			$this->ggl("Suami hanya boleh dipilih sekali (1)");
		}
		if ($this->iJumlahIbu > 1) {
			$this->ggl("Ibu hanya boleh dipilih sekali (1)");
		}
		if ($this->iJumlahKakek > 1) {
			$this->ggl("Kakek hanya boleh dipilih sekali (1)");
		}
		if ($this->iJumlahNenekBapak > 1) {
			$this->ggl("Nenek(Ibunya Bapak) hanya boleh dipilih sekali (1)");
		}
		if ($this->iJumlahNenekIbu > 1) {
			$this->ggl("Nenek(Ibunya Ibu) hanya boleh dipilih sekali (1)");
		}
		if ($this->iJumlahNenekKakek > 1) {
			$this->ggl("Nenek(Ibunya Kakek) hanya boleh dipilih sekali (1)");
		}
		if ($this->iJumlahPriaMerdekakan > 1) {
			$this->ggl("Pria yang memerdekakan budak hanya boleh dipilih sekali (1)");
		}
		if ($this->iJumlahWanitaMerdekakan > 1) {
			$this->ggl("Wanita yang memerdekakan budak hanya boleh dipilih sekali (1)");
		}
		if($this->iJumlahIstri>4){
			$this->ggl("Jumlah Istri tidak boleh lebih dari 4 dalam satu masa");
		}
		if (($this->iJumlahSuami == 1) && ($this->iJumlahIstri > 0)) {
			$this->ggl("Siapa yang meninggal? Suami atau Istri?");
		}
	}

	public function hitung()
	{
		$this->cek();

		if ($this->iJumlahAnakLaki == 0 && $this->iJumlahAnakPerempuan == 0) {
			// Masalah 'umariyatain
			if($this->iJumlahCucuLaki == 0 && $this->iJumlahCucuPerempuan ==0){
				if ($this->iJumlahSuami == 1) {
					$this->iJthSuami	= round($this->iHarta/2);
					if($this->iJumlahIbu == 1){
						$this->iJthIbu		= round($this->iHarta/6)*$this->iJumlahIbu;
					}
					if($this->iJumlahIbu == 0){
						if($this->iJumlahNenekBapak == 1 && $this->iJumlahNenekIbu == 0){
							$this->iJthNenekBapak = round($this->iHarta/6)*$this->iJumlahNenekBapak;
						}
						if($this->iJumlahNenekBapak == 0 && $this->iJumlahNenekIbu == 1){
							$this->iJthNenekIbu = round($this->iHarta/6)*$this->iJumlahNenekIbu;
						}
						if($this->iJumlahNenekBapak == 1 && $this->iJumlahNenekIbu == 1){
							$this->iJthNenekBapak = round($this->iHarta/12)*$this->iJumlahNenekBapak;
							$this->iJthNenekIbu = round($this->iHarta/12)*$this->iJumlahNenekIbu;
						}
						if($this->iJumlahNenekBapak == 0 && $this->iJumlahNenekIbu == 0){
							$this->iJthNenekKakek = round($this->iHarta/6)*$this->iJumlahNenekKakek;
						}
					}
					if($this->iJumlahBapak > 0){
						$this->iJthBapak	= round($this->iHarta/3);
						$this->iSisa		= $this->iHarta - ($this->iJthSuami+$this->iJthIbu+$this->iJthBapak+$this->iJthNenekBapak*$this->iJumlahNenekBapak+$this->iJthNenekIbu*$this->iJumlahNenekIbu+$this->iJthNenekKakek*$this->iJumlahNenekKakek);
						$this->iJthBapak 	= $this->iJthBapak + $this->iSisa*$this->iJumlahBapak;
						$this->iSisa		= $this->iHarta - ($this->iJthSuami + $this->iJthIbu + $this->iJthBapak+$this->iJthNenekBapak*$this->iJumlahNenekBapak+$this->iJthNenekIbu*$this->iJumlahNenekIbu+$this->iJthNenekKakek*$this->iJumlahNenekKakek);
						if ($this->iJumlahSaudaraSeibu>0) 
							array_push($this->res, "Jatah tiap Saudara Seibu : 0 (karena dihalangi oleh Bapak)");
						if ($this->iJumlahSaudaraKandung>0) 
							array_push($this->res, "Jatah tiap Saudara Kandung : 0 (karena dihalangi oleh Bapak)");
						if ($this->iJumlahSaudariKandung>0) 
							array_push($this->res, "Jatah tiap Saudari Kandung : 0 (karena dihalangi oleh Bapak)");
						if ($this->iJumlahSaudariSeibu>0) 
							array_push($this->res, "Jatah tiap Saudari Seibu : 0 (karena dihalangi oleh Bapak)");
						if ($this->iJumlahSaudaraSebapak>0) 
							array_push($this->res, "Jatah tiap Saudara Sebapak : 0 (karena dihalangi oleh Bapak)");
						if ($this->iJumlahSaudariSebapak>0) 
							array_push($this->res, "Jatah tiap Saudari Sebapak : 0 (karena dihalangi oleh Bapak)");
						if ($this->iJumlahPutraSaudaraKandung>0) 
							array_push($this->res, "Jatah tiap Putra Saudara Kandung : 0 (karena dihalangi oleh Bapak)");
						if ($this->iJumlahPutraSaudaraSebapak>0) 
							array_push($this->res, "Jatah tiap Putra Saudara Sebapak : 0 (karena dihalangi oleh Bapak)");
						if ($this->iJumlahPamanKandung>0) 
							array_push($this->res, "Jatah tiap Paman Kandung : 0 (karena dihalangi oleh Bapak)");
						if ($this->iJumlahPamanSebapak>0) 
							array_push($this->res, "Jatah tiap Paman Sebapak : 0 (karena dihalangi oleh Bapak)");
						if ($this->iJumlahPutraPamanKandung>0) 
							array_push($this->res, "Jatah tiap Putra Paman Kandung : 0 (karena dihalangi oleh Bapak)");
						if ($this->iJumlahPutraPamanSebapak>0) 
							array_push($this->res, "Jatah tiap Putra Paman Sebapak : 0 (karena dihalangi oleh Bapak)");
						if ($this->iJumlahPriaMerdekakan>0) 
							array_push($this->res, "Jatah Pria yang Memerdekakan Budak : 0 (karena dihalangi oleh Bapak)");
						if ($this->iJumlahWanitaMerdekakan>0) 
							array_push($this->res, "Jatah Wanita yang Memerdekakan Budak : 0 (karena dihalangi oleh Bapak)");
					}
					if($this->iJumlahBapak == 0){
						if($this->iJumlahKakek == 1){
							$this->iJthKakek	= round($this->iHarta/3)*$this->iJumlahKakek;
							$this->iSisa		= $this->iHarta - ($this->iJthSuami+$this->iJthIbu+$this->iJthKakek+$this->iJthNenekBapak*$this->iJumlahNenekBapak+$this->iJthNenekIbu*$this->iJumlahNenekIbu+$this->iJthNenekKakek*$this->iJumlahNenekKakek);
							$this->iJthKakek 	= $this->iJthKakek + $this->iSisa*$this->iJumlahKakek;
							$this->iSisa		= $this->iHarta - ($this->iJthSuami + $this->iJthIbu + $this->iJthKakek+$this->iJthNenekBapak*$this->iJumlahNenekBapak+$this->iJthNenekIbu*$this->iJumlahNenekIbu+$this->iJthNenekKakek*$this->iJumlahNenekKakek);
							if ($this->iJumlahSaudaraSeibu>0) 
								array_push($this->res, "Jatah tiap Saudara Seibu : 0 (karena dihalangi oleh Kakek)");
							if ($this->iJumlahSaudaraKandung>0) 
								array_push($this->res, "Jatah tiap Saudara Kandung : 0 (karena dihalangi oleh Kakek)");
							if ($this->iJumlahSaudariKandung>0) 
								array_push($this->res, "Jatah tiap Saudari Kandung : 0 (karena dihalangi oleh Kakek)");
							if ($this->iJumlahSaudariSeibu>0) 
								array_push($this->res, "Jatah tiap Saudari Seibu : 0 (karena dihalangi oleh Kakek)");
							if ($this->iJumlahSaudaraSebapak>0) 
								array_push($this->res, "Jatah tiap Saudara Sebapak : 0 (karena dihalangi oleh Kakek)");
							if ($this->iJumlahSaudariSebapak>0) 
								array_push($this->res, "Jatah tiap Saudari Sebapak : 0 (karena dihalangi oleh Kakek)");
							if ($this->iJumlahPutraSaudaraKandung>0) 
								array_push($this->res, "Jatah tiap Putra Saudara Kandung : 0 (karena dihalangi oleh Kakek)");
							if ($this->iJumlahPutraSaudaraSebapak>0) 
								array_push($this->res, "Jatah tiap Putra Saudara Sebapak : 0 (karena dihalangi oleh Kakek)");
							if ($this->iJumlahPamanKandung>0) 
								array_push($this->res, "Jatah tiap Paman Kandung : 0 (karena dihalangi oleh Kakek)");
							if ($this->iJumlahPamanSebapak>0) 
								array_push($this->res, "Jatah tiap Paman Sebapak : 0 (karena dihalangi oleh Kakek)");
							if ($this->iJumlahPutraPamanKandung>0) 
								array_push($this->res, "Jatah tiap Putra Paman Kandung : 0 (karena dihalangi oleh Kakek)");
							if ($this->iJumlahPutraPamanSebapak>0) 
								array_push($this->res, "Jatah tiap Putra Paman Sebapak : 0 (karena dihalangi oleh Kakek)");
							if ($this->iJumlahPriaMerdekakan>0) 
								array_push($this->res, "Jatah Pria yang Memerdekakan Budak : 0 (karena dihalangi oleh Kakek)");
							if ($this->iJumlahWanitaMerdekakan>0) 
								array_push($this->res, "Jatah Wanita yang Memerdekakan Budak : 0 (karena dihalangi oleh Kakek)");
						}
						if($this->iJumlahKakek == 0){
							if($this->iJumlahSaudaraSeibu == 1){
								$this->iJthSaudaraSeibu = round($this->iHarta/6);
								$this->iSisa		= $this->iHarta - ($this->iJthSuami+$this->iJthIbu+$this->iJthNenekBapak*$this->iJumlahNenekBapak+$this->iJthNenekIbu*$this->iJumlahNenekIbu+$this->iJthNenekKakek*$this->iJumlahNenekKakek+$this->iJthSaudaraSeibu);
								array_push($this->res, "Jatah tiap Saudara Seibu (1/6) : " . $this->tandaPemisahTitik($this->iJthSaudaraSeibu));
							}
							if($this->iJumlahSaudaraSeibu > 1){
								$this->iJthSaudaraSeibu = round($this->iHarta/3);
								$this->iSisa		= $this->iHarta - ($this->iJthSuami+$this->iJthIbu+$this->iJthNenekBapak*$this->iJumlahNenekBapak+$this->iJthNenekIbu*$this->iJumlahNenekIbu+$this->iJthNenekKakek*$this->iJumlahNenekKakek+$this->iJthSaudaraSeibu);
								array_push($this->res, "Jatah tiap Saudara Seibu (1/3) : " . $this->tandaPemisahTitik($this->iJthSaudaraSeibu/$this->iJumlahSaudaraSeibu));
							}
							if($this->iJumlahSaudaraKandung > 0){
								$this->iSisa		= $this->iHarta - ($this->iJthSuami+$this->iJthIbu+$this->iJthNenekBapak*$this->iJumlahNenekBapak+$this->iJthNenekIbu*$this->iJumlahNenekIbu+$this->iJthNenekKakek*$this->iJumlahNenekKakek+$this->iJthSaudaraSeibu);
								if($this->iJumlahSaudariKandung == 0){
									$this->iJthSaudaraKandung = round($this->iSisa/$this->iJumlahSaudaraKandung);
								}
								if($this->iJumlahSaudariKandung > 0){
									$this->iJthSaudaraKandung = round($this->iSisa/($this->iJumlahSaudaraKandung + $this->iJumlahSaudariKandung));
									$this->iJthSaudariKandung = $this->iJthSaudaraKandung;
									array_push($this->res, "Jatah tiap Saudari Kandung (Sisa) : " . $this->tandaPemisahTitik($this->iJthSaudariKandung));
								}
								$this->iSisa		= $this->iHarta - ($this->iJthSuami+$this->iJthIbu+$this->iJthNenekBapak*$this->iJumlahNenekBapak+$this->iJthNenekIbu*$this->iJumlahNenekIbu+$this->iJthNenekKakek*$this->iJumlahNenekKakek+$this->iJthSaudaraSeibu+$this->iJthSaudaraKandung*$this->iJumlahSaudaraKandung+$this->iJthSaudariKandung*$this->iJumlahSaudariKandung);
								array_push($this->res, "Jatah tiap Saudara Kandung (Sisa) : " . $this->tandaPemisahTitik($this->iJthSaudaraKandung));
								if($this->iJumlahSaudaraSebapak > 0) array_push($this->res, "Jatah tiap Saudara Sebapak : 0 (Karena dihalangi Saudara Sekandung)");
								if($this->iJumlahPutraSaudaraKandung > 0) array_push($this->res, "Jatah tiap Putra dari Saudara Kandung : 0 (Karena dihalangi Saudara Sekandung)");
								if($this->iJumlahPutraSaudaraSebapak > 0) array_push($this->res, "Jatah tiap Putra dari Saudara Sebapak : 0 (Karena dihalangi Saudara Sekandung)");
								if($this->iJumlahPamanKandung > 0) array_push($this->res, "Jatah tiap Paman Sekandung : 0 (Karena dihalangi Saudara Sekandung)");
								if($this->iJumlahPamanSebapak > 0) array_push($this->res, "Jatah tiap Paman Sebapak : 0 (Karena dihalangi Saudara Sekandung)");
								if($this->iJumlahPutraPamanKandung > 0) array_push($this->res, "Jatah tiap Putra dari Paman Kandung : 0 (Karena dihalangi Saudara Sekandung)");
								if($this->iJumlahPutraPamanSebapak > 0) array_push($this->res, "Jatah tiap Putra dari Paman Sebapak : 0 (Karena dihalangi Saudara Sekandung)");
								if($this->iJumlahPriaMerdekakan > 0) array_push($this->res, "Jatah tiap Pria yang Memerdekakan Budak : 0 (Karena dihalangi Saudara Sekandung)");
								if($this->iJumlahWanitaMerdekakan > 0) array_push($this->res, "Jatah tiap Wanita yang Memerdekakan Budak : 0 (Karena dihalangi Saudara Sekandung)");
								if($this->iJumlahSaudariSebapak > 0) array_push($this->res, "Jatah tiap Saudari Sebapak : 0 (Karena dihalangi Saudara Sekandung)");
							}
							if($this->iJumlahSaudaraKandung == 0){
								if($this->iJumlahSaudariKandung > 1){
									$this->iJthSaudariKandung = round((2*$this->iHarta/3)/$this->iJumlahSaudariKandung);
									$this->iSisa		= $this->iHarta - ($this->iJthSuami+$this->iJthIbu+$this->iJthNenekBapak*$this->iJumlahNenekBapak+$this->iJthNenekIbu*$this->iJumlahNenekIbu+$this->iJthNenekKakek*$this->iJumlahNenekKakek+$this->iJthSaudaraSeibu+$this->iJthSaudariKandung*$this->iJumlahSaudariKandung);
									array_push($this->res, "Jatah tiap Saudari Kandung (2/3) : " . $this->tandaPemisahTitik($this->iJthSaudariKandung));
									if($this->iJumlahSaudariSebapak > 0 && $this->iJumlahSaudaraSebapak == 0){array_push($this->res, "Jatah tiap Saudari Sebapak : 0(Karena dihalangi 2 Saudari Kandung atau lebih)");}
								}
								if($this->iJumlahSaudariKandung == 1 || $this->iJumlahSaudariKandung == 0){
									$this->iJthSaudariKandung = round($this->iHarta/2);
									if($this->iJumlahSaudaraSebapak == 0 && $this->iJumlahSaudariSebapak == 1){
										$this->iJthSaudariSebapak = round($this->iHarta/2);
										array_push($this->res, "Jatah tiap Saudari Sebapak (1/2) : " . $this->tandaPemisahTitik($this->iJthSaudariSebapak/$this->iJumlahSaudariSebapak));
									}
									if($this->iJumlahSaudaraSebapak == 0 && $this->iJumlahSaudariSebapak > 1){
										$this->iJthSaudariSebapak = round((2*$this->iHarta/3)/$this->iJumlahSaudariSebapak);
										array_push($this->res, "Jatah tiap Saudari Sebapak (2/3) : " . $this->tandaPemisahTitik($this->iJthSaudariSebapak));
									}
									$this->iSisa		= $this->iHarta - ($this->iJthSuami+$this->iJthIbu+$this->iJthNenekBapak*$this->iJumlahNenekBapak+$this->iJthNenekIbu*$this->iJumlahNenekIbu+$this->iJthNenekKakek*$this->iJumlahNenekKakek+$this->iJthSaudaraSeibu+$this->iJthSaudariKandung*$this->iJumlahSaudariKandung+$this->iJthSaudariSebapak*$this->iJumlahSaudariSebapak);
									if($this->iJumlahSaudariKandung == 1) array_push($this->res, "Jatah tiap Saudari Kandung (1/2) : " . $this->tandaPemisahTitik($this->iJthSaudariKandung/$this->iJumlahSaudariKandung));
								}
								if($this->iJumlahSaudaraSebapak > 0){
									$this->iSisa		= $this->iHarta - ($this->iJthSuami+$this->iJthIbu+$this->iJthNenekBapak*$this->iJumlahNenekBapak+$this->iJthNenekIbu*$this->iJumlahNenekIbu+$this->iJthNenekKakek*$this->iJumlahNenekKakek+$this->iJthSaudaraSeibu);
									$this->iJthSaudaraSebapak = round($this->iSisa/($this->iJumlahSaudaraSebapak+$this->iJumlahSaudariSebapak));
									$this->iJthSaudariSebapak = $this->iJthSaudaraSebapak;
									$this->iSisa		= $this->iHarta - ($this->iJthSuami+$this->iJthIbu+$this->iJthNenekBapak*$this->iJumlahNenekBapak+$this->iJthNenekIbu*$this->iJumlahNenekIbu+$this->iJthNenekKakek*$this->iJumlahNenekKakek+$this->iJthSaudaraSeibu+$this->iJthSaudaraSebapak*$this->iJumlahSaudaraSebapak+$this->iJthSaudariSebapak*$this->iJumlahSaudariSebapak);
									array_push($this->res, "Jatah tiap Saudara Sebapak (Sisa) : " . $this->tandaPemisahTitik($this->iJthSaudaraSebapak));
									if($this->iJumlahSaudariSebapak > 0) array_push($this->res, "Jatah tiap Saudari Sebapak (Sisa) : " . $this->tandaPemisahTitik($this->iJthSaudariSebapak));
									if($this->iJumlahPutraSaudaraKandung > 0) array_push($this->res, "Jatah tiap Putra dari Saudara Kandung : 0 (Karena dihalangi Saudara Sebapak)");
									if($this->iJumlahPutraSaudaraSebapak > 0) array_push($this->res, "Jatah tiap Putra dari Saudara Sebapak : 0 (Karena dihalangi Saudara Sebapak)");
									if($this->iJumlahPamanKandung > 0) array_push($this->res, "Jatah tiap Paman Sekandung : 0 (Karena dihalangi Saudara Sebapak)");
									if($this->iJumlahPamanSebapak > 0) array_push($this->res, "Jatah tiap Paman Sebapak : 0 (Karena dihalangi Saudara Sebapak)");
									if($this->iJumlahPutraPamanKandung > 0) array_push($this->res, "Jatah tiap Putra dari Paman Kandung : 0 (Karena dihalangi Saudara Sebapak)");
									if($this->iJumlahPutraPamanSebapak > 0) array_push($this->res, "Jatah tiap Putra dari Paman Sebapak : 0 (Karena dihalangi Saudara Sebapak)");
									if($this->iJumlahPriaMerdekakan > 0) array_push($this->res, "Jatah tiap Pria yang Memerdekakan Budak : 0 (Karena dihalangi Saudara Sebapak)");
									if($this->iJumlahWanitaMerdekakan > 0) array_push($this->res, "Jatah tiap Wanita yang Memerdekakan Budak : 0 (Karena dihalangi Saudara Sebapak)");
								}
								if($this->iJumlahSaudaraSebapak == 0){
									$this->iSisa		= $this->iHarta - ($this->iJthSuami+$this->iJthIbu+$this->iJthNenekBapak*$this->iJumlahNenekBapak+$this->iJthNenekIbu*$this->iJumlahNenekIbu+$this->iJthNenekKakek*$this->iJumlahNenekKakek+$this->iJthSaudaraSeibu*$this->iJumlahSaudaraSeibu+$this->iJthSaudariKandung*$this->iJumlahSaudariKandung+$this->iJthSaudariSebapak*$this->iJumlahSaudariSebapak);
									if($this->iJumlahPutraSaudaraKandung > 0){
										$this->iJthPutraSaudaraKandung = round($this->iSisa/$this->iJumlahPutraSaudaraKandung);
										$this->iSisa		= $this->iHarta - ($this->iJthSuami+$this->iJthIbu+$this->iJthNenekBapak*$this->iJumlahNenekBapak+$this->iJthNenekIbu*$this->iJumlahNenekIbu+$this->iJthNenekKakek*$this->iJumlahNenekKakek+$this->iJthSaudaraSeibu*$this->iJumlahSaudaraSeibu+$this->iJthSaudariKandung*$this->iJumlahSaudariKandung+$this->iJthSaudariSebapak*$this->iJumlahSaudariSebapak+$this->iJthPutraSaudaraKandung*$this->iJumlahPutraSaudaraKandung);
										array_push($this->res, "Jatah tiap Putra dari Saudara Sekandung (Sisa) : " . $this->tandaPemisahTitik($this->iJthPutraSaudaraKandung));
										if($this->iJumlahPutraSaudaraSebapak > 0) array_push($this->res, "Jatah tiap Putra dari Saudara Sebapak : 0 (Karena dihalangi Putra dari Saudara Sekandung)");
										if($this->iJumlahPamanKandung > 0) array_push($this->res, "Jatah tiap Paman Sekandung : 0 (Karena dihalangi Putra dari Saudara Sekandung)");
										if($this->iJumlahPamanSebapak > 0) array_push($this->res, "Jatah tiap Paman Sebapak : 0 (Karena dihalangi Putra dari Saudara Sekandung)");
										if($this->iJumlahPutraPamanKandung > 0) array_push($this->res, "Jatah tiap Putra dari Paman Kandung : 0 (Karena dihalangi Putra dari Saudara Sekandung)");
										if($this->iJumlahPutraPamanSebapak > 0) array_push($this->res, "Jatah tiap Putra dari Paman Sebapak : 0 (Karena dihalangi Putra dari Saudara Sekandung)");
										if($this->iJumlahPriaMerdekakan > 0) array_push($this->res, "Jatah tiap Pria yang Memerdekakan Budak : 0 (Karena dihalangi Putra dari Saudara Sekandung)");
										if($this->iJumlahWanitaMerdekakan > 0) array_push($this->res, "Jatah tiap Wanita yang Memerdekakan Budak : 0 (Karena dihalangi Putra dari Saudara Sekandung)");
									}
									if($this->iJumlahPutraSaudaraKandung == 0){
										$this->iJthPutraSaudaraSebapak = round($this->iSisa/$this->iJumlahPutraSaudaraSebapak);
										if($this->iJumlahPutraSaudaraSebapak > 0){
											$this->iSisa		= $this->iHarta - ($this->iJthSuami+$this->iJthIbu+$this->iJthNenekBapak*$this->iJumlahNenekBapak+$this->iJthNenekIbu*$this->iJumlahNenekIbu+$this->iJthNenekKakek*$this->iJumlahNenekKakek+$this->iJthSaudaraSeibu*$this->iJumlahSaudaraSeibu+$this->iJthSaudariKandung*$this->iJumlahSaudariKandung+$this->iJthSaudariSebapak*$this->iJumlahSaudariSebapak+$this->iJthPutraSaudaraSebapak*$this->iJumlahPutraSaudaraSebapak);
											array_push($this->res, "Jatah tiap Putra dari Saudara Sebapak (Sisa) : " . $this->tandaPemisahTitik($this->iJthPutraSaudaraSebapak));
											if($this->iJumlahPamanKandung > 0) array_push($this->res, "Jatah tiap Paman Sekandung : 0 (Karena dihalangi Putra dari Saudara Sebapak)");
											if($this->iJumlahPamanSebapak > 0) array_push($this->res, "Jatah tiap Paman Sebapak : 0 (Karena dihalangi Putra dari Saudara Sebapak)");
											if($this->iJumlahPutraPamanKandung > 0) array_push($this->res, "Jatah tiap Putra dari Paman Kandung : 0 (Karena dihalangi Putra dari Saudara Sebapak)");
											if($this->iJumlahPutraPamanSebapak > 0) array_push($this->res, "Jatah tiap Putra dari Paman Sebapak : 0 (Karena dihalangi Putra dari Saudara Sebapak)");
											if($this->iJumlahPriaMerdekakan > 0) array_push($this->res, "Jatah tiap Pria yang Memerdekakan Budak : 0 (Karena dihalangi Putra dari Saudara Sebapak)");
											if($this->iJumlahWanitaMerdekakan > 0) array_push($this->res, "Jatah tiap Wanita yang Memerdekakan Budak : 0 (Karena dihalangi Putra dari Saudara Sebapak)");
										}
										if($this->iJumlahPutraSaudaraSebapak == 0){
											$this->iJthPamanKandung = round($this->iSisa/$this->iJumlahPamanKandung);
											if($this->iJumlahPamanKandung > 0){
												$this->iSisa		= $this->iHarta - ($this->iJthSuami+$this->iJthIbu+$this->iJthNenekBapak*$this->iJumlahNenekBapak+$this->iJthNenekIbu*$this->iJumlahNenekIbu+$this->iJthNenekKakek*$this->iJumlahNenekKakek+$this->iJthSaudaraSeibu*$this->iJumlahSaudaraSeibu+$this->iJthSaudariKandung*$this->iJumlahSaudariKandung+$this->iJthSaudariSebapak*$this->iJumlahSaudariSebapak+$this->iJthPamanKandung*$this->iJumlahPamanKandung);
												array_push($this->res, "Jatah tiap Paman Sekandung (Sisa) : " . $this->tandaPemisahTitik($this->iJthPamanKandung));
												if($this->iJumlahPamanSebapak > 0) array_push($this->res, "Jatah tiap Paman Sebapak : 0 (Karena dihalangi Paman Sekandung)");
												if($this->iJumlahPutraPamanKandung > 0) array_push($this->res, "Jatah tiap Putra dari Paman Kandung : 0 (Karena dihalangi Paman Sekandung)");
												if($this->iJumlahPutraPamanSebapak > 0) array_push($this->res, "Jatah tiap Putra dari Paman Sebapak : 0 (Karena dihalangi Paman Sekandung)");
												if($this->iJumlahPriaMerdekakan > 0) array_push($this->res, "Jatah tiap Pria yang Memerdekakan Budak : 0 (Karena dihalangi Paman Sekandung)");
												if($this->iJumlahWanitaMerdekakan > 0) array_push($this->res, "Jatah tiap Wanita yang Memerdekakan Budak : 0 (Karena dihalangi Paman Sekandung)");
											}
											if($this->iJumlahPamanKandung == 0){
												$this->iJthPamanSebapak = round($this->iSisa/$this->iJumlahPamanSebapak);
												if($this->iJumlahPamanSebapak > 0){
													$this->iSisa		= $this->iHarta - ($this->iJthSuami+$this->iJthIbu+$this->iJthNenekBapak*$this->iJumlahNenekBapak+$this->iJthNenekIbu*$this->iJumlahNenekIbu+$this->iJthNenekKakek*$this->iJumlahNenekKakek+$this->iJthSaudaraSeibu*$this->iJumlahSaudaraSeibu+$this->iJthSaudariKandung*$this->iJumlahSaudariKandung+$this->iJthSaudariSebapak*$this->iJumlahSaudariSebapak+$this->iJthPamanSebapak*$this->iJumlahPamanSebapak);
													array_push($this->res, "Jatah tiap Paman Sebapak (Sisa) : " . $this->tandaPemisahTitik($this->iJthPamanSebapak));
													if($this->iJumlahPutraPamanKandung > 0) array_push($this->res, "Jatah tiap Putra dari Paman Kandung : 0 (Karena dihalangi Paman Sebapak)");
													if($this->iJumlahPutraPamanSebapak > 0) array_push($this->res, "Jatah tiap Putra dari Paman Sebapak : 0 (Karena dihalangi Paman Sebapak)");
													if($this->iJumlahPriaMerdekakan > 0) array_push($this->res, "Jatah tiap Pria yang Memerdekakan Budak : 0 (Karena dihalangi Paman Sebapak)");
													if($this->iJumlahWanitaMerdekakan > 0) array_push($this->res, "Jatah tiap Wanita yang Memerdekakan Budak : 0 (Karena dihalangi Paman Sebapak)");
												}
												if($this->iJumlahPamanSebapak == 0){
													$this->iJthPutraPamanKandung = round($this->iSisa/$this->iJumlahPutraPamanKandung);
													if($this->iJumlahPutraPamanKandung > 0){
														$this->iSisa		= $this->iHarta - ($this->iJthSuami+$this->iJthIbu+$this->iJthNenekBapak*$this->iJumlahNenekBapak+$this->iJthNenekIbu*$this->iJumlahNenekIbu+$this->iJthNenekKakek*$this->iJumlahNenekKakek+$this->iJthSaudaraSeibu*$this->iJumlahSaudaraSeibu+$this->iJthSaudariKandung*$this->iJumlahSaudariKandung+$this->iJthSaudariSebapak*$this->iJumlahSaudariSebapak+$this->iJthPutraPamanKandung*$this->iJumlahPutraPamanKandung);
														array_push($this->res, "Jatah tiap Putra dari Paman Sekandung (Sisa) : " . $this->tandaPemisahTitik($this->iJthPutraPamanKandung));
														if($this->iJumlahPutraPamanSebapak > 0) array_push($this->res, "Jatah tiap Putra dari Paman Sebapak : 0 (Karena dihalangi Putra dari Paman Sekandung)");
														if($this->iJumlahPriaMerdekakan > 0) array_push($this->res, "Jatah tiap Pria yang Memerdekakan Budak : 0 (Karena dihalangi Putra dari Paman Sekandung)");
														if($this->iJumlahWanitaMerdekakan > 0) array_push($this->res, "Jatah tiap Wanita yang Memerdekakan Budak : 0 (Karena dihalangi Putra dari Paman Sekandung)");
													}
													if($this->iJumlahPutraPamanKandung == 0){
														$this->iJthPutraPamanSebapak = round($this->iSisa/$this->iJumlahPutraPamanSebapak);
														if($this->iJumlahPutraPamanSebapak > 0){
															$this->iSisa		= $this->iHarta - ($this->iJthSuami+$this->iJthIbu+$this->iJthNenekBapak*$this->iJumlahNenekBapak+$this->iJthNenekIbu*$this->iJumlahNenekIbu+$this->iJthNenekKakek*$this->iJumlahNenekKakek+$this->iJthSaudaraSeibu*$this->iJumlahSaudaraSeibu+$this->iJthSaudariKandung*$this->iJumlahSaudariKandung+$this->iJthSaudariSebapak*$this->iJumlahSaudariSebapak+$this->iJthPutraPamanSebapak*$this->iJumlahPutraPamanSebapak);
															array_push($this->res, "Jatah tiap Putra dari Paman Sebapak (Sisa) : " . $this->tandaPemisahTitik($this->iJthPutraPamanSebapak));
															if($this->iJumlahPriaMerdekakan > 0) array_push($this->res, "Jatah Pria yang Memerdekakan Budak : 0 (Karena dihalangi Putra dari Paman Sebapak)");
															if($this->iJumlahWanitaMerdekakan > 0) array_push($this->res, "Jatah Wanita yang Memerdekakan Budak : 0 (Karena dihalangi Putra dari Paman Sebapak)");
														}
														if($this->iJumlahPutraPamanSebapak == 0){
															$this->iJthPriaMerdekakan = round($this->iSisa/($this->iJumlahPriaMerdekakan+$this->iJumlahWanitaMerdekakan));
															$this->iJthWanitaMerdekakan = $this->iJthPriaMerdekakan;
															if($this->iJumlahPriaMerdekakan > 0){
																$this->iSisa		= $this->iHarta - ($this->iJthSuami+$this->iJthIbu+$this->iJthNenekBapak*$this->iJumlahNenekBapak+$this->iJthNenekIbu*$this->iJumlahNenekIbu+$this->iJthNenekKakek*$this->iJumlahNenekKakek+$this->iJthSaudaraSeibu*$this->iJumlahSaudaraSeibu+$this->iJthSaudariKandung*$this->iJumlahSaudariKandung+$this->iJthSaudariSebapak*$this->iJumlahSaudariSebapak+$this->iJthPriaMerdekakan*$this->iJumlahPriaMerdekakan);
																array_push($this->res, "Jatah Pria yang Memerdekakan Budak (Sisa) : " . $this->tandaPemisahTitik($this->iJthPriaMerdekakan));
															}
															if($this->iJumlahWanitaMerdekakan > 0){
																$this->iSisa		= $this->iHarta - ($this->iJthSuami+$this->iJthIbu+$this->iJthNenekBapak*$this->iJumlahNenekBapak+$this->iJthNenekIbu*$this->iJumlahNenekIbu+$this->iJthNenekKakek*$this->iJumlahNenekKakek+$this->iJthSaudaraSeibu*$this->iJumlahSaudaraSeibu+$this->iJthSaudariKandung*$this->iJumlahSaudariKandung+$this->iJthSaudariSebapak*$this->iJumlahSaudariSebapak+$this->iJthWanitaMerdekakan*$this->iJumlahWanitaMerdekakan);
																array_push($this->res, "Jatah Wanita yang Memerdekakan Budak (Sisa) : " . $this->tandaPemisahTitik($this->iJthWanitaMerdekakan));
															}
														}
													}
												}
											}
										}
									}
								}
								if($this->iJumlahSaudariSeibu == 1){
									$this->iJthSaudariSeibu = round($this->iHarta/6);
									array_push($this->res, "Jatah tiap Saudari Seibu (1/6) :" . $this->tandaPemisahTitik($this->iJthSaudariSeibu));
								}
								if($this->iJumlahSaudariSeibu > 1){
									$this->iJthSaudariSeibu = round($this->iHarta/3);
									array_push($this->res, "Jatah tiap Saudari Seibu (1/3) :" . $this->tandaPemisahTitik($this->iJthSaudariSeibu/$this->iJumlahSaudariSeibu));
								}
							}
						}
					}
				}
				if ($this->iJumlahSuami == 0 && $this->iJumlahIstri == 0) {
					if($this->iJumlahIbu == 1){
						$this->iJthIbu		= round($this->iHarta/3)*$this->iJumlahIbu;
					}
					if($this->iJumlahIbu == 0){
						if($this->iJumlahNenekBapak == 1 && $this->iJumlahNenekIbu == 0){
							$this->iJthNenekBapak = round($this->iHarta/6)*$this->iJumlahNenekBapak;
						}
						if($this->iJumlahNenekBapak == 0 && $this->iJumlahNenekIbu == 1){
							$this->iJthNenekIbu = round($this->iHarta/6)*$this->iJumlahNenekIbu;
						}
						if($this->iJumlahNenekBapak == 1 && $this->iJumlahNenekIbu == 1){
							$this->iJthNenekBapak = round($this->iHarta/12)*$this->iJumlahNenekBapak;
							$this->iJthNenekIbu = round($this->iHarta/12)*$this->iJumlahNenekIbu;
						}
						if($this->iJumlahNenekBapak == 0 && $this->iJumlahNenekIbu == 0){
							$this->iJthNenekKakek = round($this->iHarta/6)*$this->iJumlahNenekKakek;
						}
					}
					if($this->iJumlahBapak > 0){
						$this->iJthBapak	= round($this->iHarta/3);
						$this->iSisa		= $this->iHarta - ($this->iJthIbu+$this->iJthBapak+$this->iJthNenekBapak*$this->iJumlahNenekBapak+$this->iJthNenekIbu*$this->iJumlahNenekIbu+$this->iJthNenekKakek*$this->iJumlahNenekKakek);
						$this->iJthBapak 	= $this->iJthBapak + $this->iSisa*$this->iJumlahBapak;
						$this->iSisa		= $this->iHarta - ($this->iJthIbu + $this->iJthBapak+$this->iJthNenekBapak*$this->iJumlahNenekBapak+$this->iJthNenekIbu*$this->iJumlahNenekIbu+$this->iJthNenekKakek*$this->iJumlahNenekKakek);
						if ($this->iJumlahSaudaraSeibu>0) 
							array_push($this->res, "Jatah tiap Saudara Seibu : 0 (karena dihalangi oleh Bapak)");
						if ($this->iJumlahSaudaraKandung>0) 
							array_push($this->res, "Jatah tiap Saudara Kandung : 0 (karena dihalangi oleh Bapak)");
						if ($this->iJumlahSaudariKandung>0) 
							array_push($this->res, "Jatah tiap Saudari Kandung : 0 (karena dihalangi oleh Bapak)");
						if ($this->iJumlahSaudariSeibu>0) 
							array_push($this->res, "Jatah tiap Saudari Seibu : 0 (karena dihalangi oleh Bapak)");
						if ($this->iJumlahSaudaraSebapak>0) 
							array_push($this->res, "Jatah tiap Saudara Sebapak : 0 (karena dihalangi oleh Bapak)");
						if ($this->iJumlahSaudariSebapak>0) 
							array_push($this->res, "Jatah tiap Saudari Sebapak : 0 (karena dihalangi oleh Bapak)");
						if ($this->iJumlahPutraSaudaraKandung>0) 
							array_push($this->res, "Jatah tiap Putra Saudara Kandung : 0 (karena dihalangi oleh Bapak)");
						if ($this->iJumlahPutraSaudaraSebapak>0) 
							array_push($this->res, "Jatah tiap Putra Saudara Sebapak : 0 (karena dihalangi oleh Bapak)");
						if ($this->iJumlahPamanKandung>0) 
							array_push($this->res, "Jatah tiap Paman Kandung : 0 (karena dihalangi oleh Bapak)");
						if ($this->iJumlahPamanSebapak>0) 
							array_push($this->res, "Jatah tiap Paman Sebapak : 0 (karena dihalangi oleh Bapak)");
						if ($this->iJumlahPutraPamanKandung>0) 
							array_push($this->res, "Jatah tiap Putra Paman Kandung : 0 (karena dihalangi oleh Bapak)");
						if ($this->iJumlahPutraPamanSebapak>0) 
							array_push($this->res, "Jatah tiap Putra Paman Sebapak : 0 (karena dihalangi oleh Bapak)");
						if ($this->iJumlahPriaMerdekakan>0) 
							array_push($this->res, "Jatah Pria yang Memerdekakan Budak : 0 (karena dihalangi oleh Bapak)");
						if ($this->iJumlahWanitaMerdekakan>0) 
							array_push($this->res, "Jatah Wanita yang Memerdekakan Budak : 0 (karena dihalangi oleh Bapak)");
					}
					if($this->iJumlahBapak == 0){
						if($this->iJumlahKakek == 1){
							$this->iJthKakek	= round($this->iHarta/3)*$this->iJumlahKakek;
							$this->iSisa		= $this->iHarta - ($this->iJthIbu+$this->iJthKakek+$this->iJthNenekBapak*$this->iJumlahNenekBapak+$this->iJthNenekIbu*$this->iJumlahNenekIbu+$this->iJthNenekKakek*$this->iJumlahNenekKakek);
							$this->iJthKakek 	= $this->iJthKakek + $this->iSisa*$this->iJumlahKakek;
							$this->iSisa		= $this->iHarta - ($this->iJthIbu + $this->iJthKakek+$this->iJthNenekBapak*$this->iJumlahNenekBapak+$this->iJthNenekIbu*$this->iJumlahNenekIbu+$this->iJthNenekKakek*$this->iJumlahNenekKakek);
							if ($this->iJumlahSaudaraSeibu>0) 
								array_push($this->res, "Jatah tiap Saudara Seibu : 0 (karena dihalangi oleh Kakek)");
							if ($this->iJumlahSaudaraKandung>0) 
								array_push($this->res, "Jatah tiap Saudara Kandung : 0 (karena dihalangi oleh Kakek)");
							if ($this->iJumlahSaudariKandung>0) 
								array_push($this->res, "Jatah tiap Saudari Kandung : 0 (karena dihalangi oleh Kakek)");
							if ($this->iJumlahSaudariSeibu>0) 
								array_push($this->res, "Jatah tiap Saudari Seibu : 0 (karena dihalangi oleh Kakek)");
							if ($this->iJumlahSaudaraSebapak>0) 
								array_push($this->res, "Jatah tiap Saudara Sebapak : 0 (karena dihalangi oleh Kakek)");
							if ($this->iJumlahSaudariSebapak>0) 
								array_push($this->res, "Jatah tiap Saudari Sebapak : 0 (karena dihalangi oleh Kakek)");
							if ($this->iJumlahPutraSaudaraKandung>0) 
								array_push($this->res, "Jatah tiap Putra Saudara Kandung : 0 (karena dihalangi oleh Kakek)");
							if ($this->iJumlahPutraSaudaraSebapak>0) 
								array_push($this->res, "Jatah tiap Putra Saudara Sebapak : 0 (karena dihalangi oleh Kakek)");
							if ($this->iJumlahPamanKandung>0) 
								array_push($this->res, "Jatah tiap Paman Kandung : 0 (karena dihalangi oleh Kakek)");
							if ($this->iJumlahPamanSebapak>0) 
								array_push($this->res, "Jatah tiap Paman Sebapak : 0 (karena dihalangi oleh Kakek)");
							if ($this->iJumlahPutraPamanKandung>0) 
								array_push($this->res, "Jatah tiap Putra Paman Kandung : 0 (karena dihalangi oleh Kakek)");
							if ($this->iJumlahPutraPamanSebapak>0) 
								array_push($this->res, "Jatah tiap Putra Paman Sebapak : 0 (karena dihalangi oleh Kakek)");
							if ($this->iJumlahPriaMerdekakan>0) 
								array_push($this->res, "Jatah Pria yang Memerdekakan Budak : 0 (karena dihalangi oleh Kakek)");
							if ($this->iJumlahWanitaMerdekakan>0) 
								array_push($this->res, "Jatah Wanita yang Memerdekakan Budak : 0 (karena dihalangi oleh Kakek)");
						}
						if($this->iJumlahKakek == 0){
							if($this->iJumlahSaudaraSeibu == 1){
								$this->iJthSaudaraSeibu = round($this->iHarta/6);
								$this->iSisa		= $this->iHarta - ($this->iJthIbu+$this->iJthNenekBapak*$this->iJumlahNenekBapak+$this->iJthNenekIbu*$this->iJumlahNenekIbu+$this->iJthNenekKakek*$this->iJumlahNenekKakek+$this->iJthSaudaraSeibu);
								array_push($this->res, "Jatah tiap Saudara Seibu (1/6) : " . $this->tandaPemisahTitik($this->iJthSaudaraSeibu));
							}
							if($this->iJumlahSaudaraSeibu > 1){
								$this->iJthSaudaraSeibu = round($this->iHarta/3);
								$this->iSisa		= $this->iHarta - ($this->iJthIbu+$this->iJthNenekBapak*$this->iJumlahNenekBapak+$this->iJthNenekIbu*$this->iJumlahNenekIbu+$this->iJthNenekKakek*$this->iJumlahNenekKakek+$this->iJthSaudaraSeibu);
								array_push($this->res, "Jatah tiap Saudara Seibu (1/3) : " . $this->tandaPemisahTitik($this->iJthSaudaraSeibu/$this->iJumlahSaudaraSeibu));
							}
							if($this->iJumlahSaudaraKandung > 0){
								$this->iSisa		= $this->iHarta - ($this->iJthSuami+$this->iJthIbu+$this->iJthNenekBapak*$this->iJumlahNenekBapak+$this->iJthNenekIbu*$this->iJumlahNenekIbu+$this->iJthNenekKakek*$this->iJumlahNenekKakek+$this->iJthSaudaraSeibu);
								if($this->iJumlahSaudariKandung == 0){
									$this->iJthSaudaraKandung = round($this->iSisa/$this->iJumlahSaudaraKandung);
								}
								if($this->iJumlahSaudariKandung > 0){
									$this->iJthSaudaraKandung = round($this->iSisa/($this->iJumlahSaudaraKandung + $this->iJumlahSaudariKandung));
									$this->iJthSaudariKandung = $this->iJthSaudaraKandung;
									array_push($this->res, "Jatah tiap Saudari Kandung (Sisa) : " . $this->tandaPemisahTitik($this->iJthSaudariKandung));
								}
								$this->iSisa		= $this->iHarta - ($this->iJthSuami+$this->iJthIbu+$this->iJthNenekBapak*$this->iJumlahNenekBapak+$this->iJthNenekIbu*$this->iJumlahNenekIbu+$this->iJthNenekKakek*$this->iJumlahNenekKakek+$this->iJthSaudaraSeibu+$this->iJthSaudaraKandung*$this->iJumlahSaudaraKandung+$this->iJthSaudariKandung*$this->iJumlahSaudariKandung);
								array_push($this->res, "Jatah tiap Saudara Kandung (Sisa) : " . $this->tandaPemisahTitik($this->iJthSaudaraKandung));
								if($this->iJumlahSaudaraSebapak > 0) array_push($this->res, "Jatah tiap Saudara Sebapak : 0 (Karena dihalangi Saudara Sekandung)");
								if($this->iJumlahSaudaraSebapak > 0) array_push($this->res, "Jatah tiap Saudara Sebapak : 0 (Karena dihalangi Saudara Sekandung)");
								if($this->iJumlahPutraSaudaraKandung > 0) array_push($this->res, "Jatah tiap Putra dari Saudara Kandung : 0 (Karena dihalangi Saudara Sekandung)");
								if($this->iJumlahPutraSaudaraSebapak > 0) array_push($this->res, "Jatah tiap Putra dari Saudara Sebapak : 0 (Karena dihalangi Saudara Sekandung)");
								if($this->iJumlahPamanKandung > 0) array_push($this->res, "Jatah tiap Paman Sekandung : 0 (Karena dihalangi Saudara Sekandung)");
								if($this->iJumlahPamanSebapak > 0) array_push($this->res, "Jatah tiap Paman Sebapak : 0 (Karena dihalangi Saudara Sekandung)");
								if($this->iJumlahPutraPamanKandung > 0) array_push($this->res, "Jatah tiap Putra dari Paman Kandung : 0 (Karena dihalangi Saudara Sekandung)");
								if($this->iJumlahPutraPamanSebapak > 0) array_push($this->res, "Jatah tiap Putra dari Paman Sebapak : 0 (Karena dihalangi Saudara Sekandung)");
								if($this->iJumlahPriaMerdekakan > 0) array_push($this->res, "Jatah tiap Pria yang Memerdekakan Budak : 0 (Karena dihalangi Saudara Sekandung)");
								if($this->iJumlahWanitaMerdekakan > 0) array_push($this->res, "Jatah tiap Wanita yang Memerdekakan Budak : 0 (Karena dihalangi Saudara Sekandung)");
								if($this->iJumlahSaudariSebapak > 0) array_push($this->res, "Jatah tiap Saudari Sebapak : 0 (Karena dihalangi Saudara Sekandung)");
							}
							if($this->iJumlahSaudaraKandung == 0){
								if($this->iJumlahSaudariKandung > 1){
									$this->iJthSaudariKandung = round((2*$this->iHarta/3)/$this->iJumlahSaudariKandung);
									$this->iSisa		= $this->iHarta - ($this->iJthSuami+$this->iJthIbu+$this->iJthNenekBapak*$this->iJumlahNenekBapak+$this->iJthNenekIbu*$this->iJumlahNenekIbu+$this->iJthNenekKakek*$this->iJumlahNenekKakek+$this->iJthSaudaraSeibu+$this->iJthSaudariKandung*$this->iJumlahSaudariKandung);
									array_push($this->res, "Jatah tiap Saudari Kandung (2/3) : " . $this->tandaPemisahTitik($this->iJthSaudariKandung));
									if($this->iJumlahSaudariSebapak > 0 && $this->iJumlahSaudaraSebapak == 0){array_push($this->res, "Jatah tiap Saudari Sebapak : 0(Karena dihalangi 2 Saudari Kandung atau lebih)");}
								}
								if($this->iJumlahSaudariKandung == 1 || $this->iJumlahSaudariKandung == 0){
									$this->iJthSaudariKandung = round($this->iHarta/2);
									if($this->iJumlahSaudaraSebapak == 0 && $this->iJumlahSaudariSebapak == 1){
										$this->iJthSaudariSebapak = round($this->iHarta/2);
										array_push($this->res, "Jatah tiap Saudari Sebapak (1/2) : " . $this->tandaPemisahTitik($this->iJthSaudariSebapak/$this->iJumlahSaudariSebapak));
									}
									if($this->iJumlahSaudaraSebapak == 0 && $this->iJumlahSaudariSebapak > 1){
										$this->iJthSaudariSebapak = round((2*$this->iHarta/3)/$this->iJumlahSaudariSebapak);
										array_push($this->res, "Jatah tiap Saudari Sebapak (2/3) : " . $this->tandaPemisahTitik($this->iJthSaudariSebapak));
									}
									$this->iSisa		= $this->iHarta - ($this->iJthSuami+$this->iJthIbu+$this->iJthNenekBapak*$this->iJumlahNenekBapak+$this->iJthNenekIbu*$this->iJumlahNenekIbu+$this->iJthNenekKakek*$this->iJumlahNenekKakek+$this->iJthSaudaraSeibu+$this->iJthSaudariKandung*$this->iJumlahSaudariKandung+$this->iJthSaudariSebapak*$this->iJumlahSaudariSebapak);
									if($this->iJumlahSaudariKandung == 1) array_push($this->res, "Jatah tiap Saudari Kandung (1/2) : " . $this->tandaPemisahTitik($this->iJthSaudariKandung/$this->iJumlahSaudariKandung));
								}
								if($this->iJumlahSaudaraSebapak > 0){
									$this->iSisa		= $this->iHarta - ($this->iJthIbu+$this->iJthNenekBapak*$this->iJumlahNenekBapak+$this->iJthNenekIbu*$this->iJumlahNenekIbu+$this->iJthNenekKakek*$this->iJumlahNenekKakek+$this->iJthSaudaraSeibu);
									$this->iJthSaudaraSebapak = round($this->iSisa/($this->iJumlahSaudaraSebapak+$this->iJumlahSaudariSebapak));
									$this->iJthSaudariSebapak = $this->iJthSaudaraSebapak;
									$this->iSisa		= $this->iHarta - ($this->iJthIbu+$this->iJthNenekBapak*$this->iJumlahNenekBapak+$this->iJthNenekIbu*$this->iJumlahNenekIbu+$this->iJthNenekKakek*$this->iJumlahNenekKakek+$this->iJthSaudaraSeibu+$this->iJthSaudaraSebapak*$this->iJumlahSaudaraSebapak+$this->iJthSaudariSebapak*$this->iJumlahSaudariSebapak);
									array_push($this->res, "Jatah tiap Saudara Sebapak (Sisa) : " . $this->tandaPemisahTitik($this->iJthSaudaraSebapak));
									if($this->iJumlahSaudariSebapak > 0) array_push($this->res, "Jatah tiap Saudari Sebapak : " . $this->tandaPemisahTitik($this->iJthSaudariSebapak));
									if($this->iJumlahPutraSaudaraKandung > 0) array_push($this->res, "Jatah tiap Putra dari Saudara Kandung : 0 (Karena dihalangi Saudara Sebapak)");
									if($this->iJumlahPutraSaudaraSebapak > 0) array_push($this->res, "Jatah tiap Putra dari Saudara Sebapak : 0 (Karena dihalangi Saudara Sebapak)");
									if($this->iJumlahPamanKandung > 0) array_push($this->res, "Jatah tiap Paman Sekandung : 0 (Karena dihalangi Saudara Sebapak)");
									if($this->iJumlahPamanSebapak > 0) array_push($this->res, "Jatah tiap Paman Sebapak : 0 (Karena dihalangi Saudara Sebapak)");
									if($this->iJumlahPutraPamanKandung > 0) array_push($this->res, "Jatah tiap Putra dari Paman Kandung : 0 (Karena dihalangi Saudara Sebapak)");
									if($this->iJumlahPutraPamanSebapak > 0) array_push($this->res, "Jatah tiap Putra dari Paman Sebapak : 0 (Karena dihalangi Saudara Sebapak)");
									if($this->iJumlahPriaMerdekakan > 0) array_push($this->res, "Jatah tiap Pria yang Memerdekakan Budak : 0 (Karena dihalangi Saudara Sebapak)");
									if($this->iJumlahWanitaMerdekakan > 0) array_push($this->res, "Jatah tiap Wanita yang Memerdekakan Budak : 0 (Karena dihalangi Saudara Sebapak)");
								}
								if($this->iJumlahSaudaraSebapak == 0){
									$this->iSisa		= $this->iHarta - ($this->iJthIbu+$this->iJthNenekBapak*$this->iJumlahNenekBapak+$this->iJthNenekIbu*$this->iJumlahNenekIbu+$this->iJthNenekKakek*$this->iJumlahNenekKakek+$this->iJthSaudaraSeibu*$this->iJumlahSaudaraSeibu+$this->iJthSaudariKandung*$this->iJumlahSaudariKandung+$this->iJthSaudariSebapak*$this->iJumlahSaudariSebapak);
									if($this->iJumlahPutraSaudaraKandung > 0){
										$this->iJthPutraSaudaraKandung = round($this->iSisa/$this->iJumlahPutraSaudaraKandung);
										$this->iSisa		= $this->iHarta - ($this->iJthIbu+$this->iJthNenekBapak*$this->iJumlahNenekBapak+$this->iJthNenekIbu*$this->iJumlahNenekIbu+$this->iJthNenekKakek*$this->iJumlahNenekKakek+$this->iJthSaudaraSeibu*$this->iJumlahSaudaraSeibu+$this->iJthSaudariKandung*$this->iJumlahSaudariKandung+$this->iJthSaudariSebapak*$this->iJumlahSaudariSebapak+$this->iJthPutraSaudaraKandung*$this->iJumlahPutraSaudaraKandung);
										array_push($this->res, "Jatah tiap Putra dari Saudara Sekandung (Sisa) : " . $this->tandaPemisahTitik($this->iJthPutraSaudaraKandung));
										if($this->iJumlahPutraSaudaraSebapak > 0) array_push($this->res, "Jatah tiap Putra dari Saudara Sebapak : 0 (Karena dihalangi Putra dari Saudara Sekandung)");
										if($this->iJumlahPamanKandung > 0) array_push($this->res, "Jatah tiap Paman Sekandung : 0 (Karena dihalangi Putra dari Saudara Sekandung)");
										if($this->iJumlahPamanSebapak > 0) array_push($this->res, "Jatah tiap Paman Sebapak : 0 (Karena dihalangi Putra dari Saudara Sekandung)");
										if($this->iJumlahPutraPamanKandung > 0) array_push($this->res, "Jatah tiap Putra dari Paman Kandung : 0 (Karena dihalangi Putra dari Saudara Sekandung)");
										if($this->iJumlahPutraPamanSebapak > 0) array_push($this->res, "Jatah tiap Putra dari Paman Sebapak : 0 (Karena dihalangi Putra dari Saudara Sekandung)");
										if($this->iJumlahPriaMerdekakan > 0) array_push($this->res, "Jatah tiap Pria yang Memerdekakan Budak : 0 (Karena dihalangi Putra dari Saudara Sekandung)");
										if($this->iJumlahWanitaMerdekakan > 0) array_push($this->res, "Jatah tiap Wanita yang Memerdekakan Budak : 0 (Karena dihalangi Putra dari Saudara Sekandung)");
									}
									if($this->iJumlahPutraSaudaraKandung == 0){
										$this->iJthPutraSaudaraSebapak = round($this->iSisa/$this->iJumlahPutraSaudaraSebapak);
										if($this->iJumlahPutraSaudaraSebapak > 0){
											$this->iSisa		= $this->iHarta - ($this->iJthIbu+$this->iJthNenekBapak*$this->iJumlahNenekBapak+$this->iJthNenekIbu*$this->iJumlahNenekIbu+$this->iJthNenekKakek*$this->iJumlahNenekKakek+$this->iJthSaudaraSeibu*$this->iJumlahSaudaraSeibu+$this->iJthSaudariKandung*$this->iJumlahSaudariKandung+$this->iJthSaudariSebapak*$this->iJumlahSaudariSebapak+$this->iJthPutraSaudaraSebapak*$this->iJumlahPutraSaudaraSebapak);
											array_push($this->res, "Jatah tiap Putra dari Saudara Sebapak (Sisa) : " . $this->tandaPemisahTitik($this->iJthPutraSaudaraSebapak));
											if($this->iJumlahPamanKandung > 0) array_push($this->res, "Jatah tiap Paman Sekandung : 0 (Karena dihalangi Putra dari Saudara Sebapak)");
											if($this->iJumlahPamanSebapak > 0) array_push($this->res, "Jatah tiap Paman Sebapak : 0 (Karena dihalangi Putra dari Saudara Sebapak)");
											if($this->iJumlahPutraPamanKandung > 0) array_push($this->res, "Jatah tiap Putra dari Paman Kandung : 0 (Karena dihalangi Putra dari Saudara Sebapak)");
											if($this->iJumlahPutraPamanSebapak > 0) array_push($this->res, "Jatah tiap Putra dari Paman Sebapak : 0 (Karena dihalangi Putra dari Saudara Sebapak)");
											if($this->iJumlahPriaMerdekakan > 0) array_push($this->res, "Jatah tiap Pria yang Memerdekakan Budak : 0 (Karena dihalangi Putra dari Saudara Sebapak)");
											if($this->iJumlahWanitaMerdekakan > 0) array_push($this->res, "Jatah tiap Wanita yang Memerdekakan Budak : 0 (Karena dihalangi Putra dari Saudara Sebapak)");
										}
										if($this->iJumlahPutraSaudaraSebapak == 0){
											$this->iJthPamanKandung = round($this->iSisa/$this->iJumlahPamanKandung);
											if($this->iJumlahPamanKandung > 0){
												$this->iSisa		= $this->iHarta - ($this->iJthIbu+$this->iJthNenekBapak*$this->iJumlahNenekBapak+$this->iJthNenekIbu*$this->iJumlahNenekIbu+$this->iJthNenekKakek*$this->iJumlahNenekKakek+$this->iJthSaudaraSeibu*$this->iJumlahSaudaraSeibu+$this->iJthSaudariKandung*$this->iJumlahSaudariKandung+$this->iJthSaudariSebapak*$this->iJumlahSaudariSebapak+$this->iJthPamanKandung*$this->iJumlahPamanKandung);
												array_push($this->res, "Jatah tiap Paman Sekandung (Sisa) : " . $this->tandaPemisahTitik($this->iJthPamanKandung));
												if($this->iJumlahPamanSebapak > 0) array_push($this->res, "Jatah tiap Paman Sebapak : 0 (Karena dihalangi Paman Sekandung)");
												if($this->iJumlahPutraPamanKandung > 0) array_push($this->res, "Jatah tiap Putra dari Paman Kandung : 0 (Karena dihalangi Paman Sekandung)");
												if($this->iJumlahPutraPamanSebapak > 0) array_push($this->res, "Jatah tiap Putra dari Paman Sebapak : 0 (Karena dihalangi Paman Sekandung)");
												if($this->iJumlahPriaMerdekakan > 0) array_push($this->res, "Jatah tiap Pria yang Memerdekakan Budak : 0 (Karena dihalangi Paman Sekandung)");
												if($this->iJumlahWanitaMerdekakan > 0) array_push($this->res, "Jatah tiap Wanita yang Memerdekakan Budak : 0 (Karena dihalangi Paman Sekandung)");
											}
											if($this->iJumlahPamanKandung == 0){
												$this->iJthPamanSebapak = round($this->iSisa/$this->iJumlahPamanSebapak);
												if($this->iJumlahPamanSebapak > 0){
													$this->iSisa		= $this->iHarta - ($this->iJthIbu+$this->iJthNenekBapak*$this->iJumlahNenekBapak+$this->iJthNenekIbu*$this->iJumlahNenekIbu+$this->iJthNenekKakek*$this->iJumlahNenekKakek+$this->iJthSaudaraSeibu*$this->iJumlahSaudaraSeibu+$this->iJthSaudariKandung*$this->iJumlahSaudariKandung+$this->iJthSaudariSebapak*$this->iJumlahSaudariSebapak+$this->iJthPamanSebapak*$this->iJumlahPamanSebapak);
													array_push($this->res, "Jatah tiap Paman Sebapak (Sisa) : " . $this->tandaPemisahTitik($this->iJthPamanSebapak));
													if($this->iJumlahPutraPamanKandung > 0) array_push($this->res, "Jatah tiap Putra dari Paman Kandung : 0 (Karena dihalangi Paman Sebapak)");
													if($this->iJumlahPutraPamanSebapak > 0) array_push($this->res, "Jatah tiap Putra dari Paman Sebapak : 0 (Karena dihalangi Paman Sebapak)");
													if($this->iJumlahPriaMerdekakan > 0) array_push($this->res, "Jatah tiap Pria yang Memerdekakan Budak : 0 (Karena dihalangi Paman Sebapak)");
													if($this->iJumlahWanitaMerdekakan > 0) array_push($this->res, "Jatah tiap Wanita yang Memerdekakan Budak : 0 (Karena dihalangi Paman Sebapak)");
												}
												if($this->iJumlahPamanSebapak == 0){
													$this->iJthPutraPamanKandung = round($this->iSisa/$this->iJumlahPutraPamanKandung);
													if($this->iJumlahPutraPamanKandung > 0){
														$this->iSisa		= $this->iHarta - ($this->iJthIbu+$this->iJthNenekBapak*$this->iJumlahNenekBapak+$this->iJthNenekIbu*$this->iJumlahNenekIbu+$this->iJthNenekKakek*$this->iJumlahNenekKakek+$this->iJthSaudaraSeibu*$this->iJumlahSaudaraSeibu+$this->iJthSaudariKandung*$this->iJumlahSaudariKandung+$this->iJthSaudariSebapak*$this->iJumlahSaudariSebapak+$this->iJthPutraPamanKandung*$this->iJumlahPutraPamanKandung);
														array_push($this->res, "Jatah tiap Putra dari Paman Sekandung (Sisa) : " . $this->tandaPemisahTitik($this->iJthPutraPamanKandung));
														if($this->iJumlahPutraPamanSebapak > 0) array_push($this->res, "Jatah tiap Putra dari Paman Sebapak : 0 (Karena dihalangi Putra dari Paman Sekandung)");
														if($this->iJumlahPriaMerdekakan > 0) array_push($this->res, "Jatah tiap Pria yang Memerdekakan Budak : 0 (Karena dihalangi Putra dari Paman Sekandung)");
														if($this->iJumlahWanitaMerdekakan > 0) array_push($this->res, "Jatah tiap Wanita yang Memerdekakan Budak : 0 (Karena dihalangi Putra dari Paman Sekandung)");
													}
													if($this->iJumlahPutraPamanKandung == 0){
														$this->iJthPutraPamanSebapak = round($this->iSisa/$this->iJumlahPutraPamanSebapak);
														if($this->iJumlahPutraPamanSebapak > 0){
															$this->iSisa		= $this->iHarta - ($this->iJthIbu+$this->iJthNenekBapak*$this->iJumlahNenekBapak+$this->iJthNenekIbu*$this->iJumlahNenekIbu+$this->iJthNenekKakek*$this->iJumlahNenekKakek+$this->iJthSaudaraSeibu*$this->iJumlahSaudaraSeibu+$this->iJthSaudariKandung*$this->iJumlahSaudariKandung+$this->iJthSaudariSebapak*$this->iJumlahSaudariSebapak+$this->iJthPutraPamanSebapak*$this->iJumlahPutraPamanSebapak);
															array_push($this->res, "Jatah tiap Putra dari Paman Sebapak (Sisa) : " . $this->tandaPemisahTitik($this->iJthPutraPamanSebapak));
															if($this->iJumlahPriaMerdekakan > 0) array_push($this->res, "Jatah Pria yang Memerdekakan Budak : 0 (Karena dihalangi Putra dari Paman Sebapak)");
															if($this->iJumlahWanitaMerdekakan > 0) array_push($this->res, "Jatah Wanita yang Memerdekakan Budak : 0 (Karena dihalangi Putra dari Paman Sebapak)");
														}
														if($this->iJumlahPutraPamanSebapak == 0){
															$this->iJthPriaMerdekakan = round($this->iSisa/($this->iJumlahPriaMerdekakan+$this->iJumlahWanitaMerdekakan));
															$this->iJthWanitaMerdekakan = $this->iJthPriaMerdekakan;
															if($this->iJumlahPriaMerdekakan > 0){
																$this->iSisa		= $this->iHarta - ($this->iJthIbu+$this->iJthNenekBapak*$this->iJumlahNenekBapak+$this->iJthNenekIbu*$this->iJumlahNenekIbu+$this->iJthNenekKakek*$this->iJumlahNenekKakek+$this->iJthSaudaraSeibu*$this->iJumlahSaudaraSeibu+$this->iJthSaudariKandung*$this->iJumlahSaudariKandung+$this->iJthSaudariSebapak*$this->iJumlahSaudariSebapak+$this->iJthPriaMerdekakan*$this->iJumlahPriaMerdekakan);
																array_push($this->res, "Jatah Pria yang Memerdekakan Budak (Sisa) : " . $this->tandaPemisahTitik($this->iJthPriaMerdekakan));
															}
															if($this->iJumlahWanitaMerdekakan > 0){
																$this->iSisa		= $this->iHarta - ($this->iJthIbu+$this->iJthNenekBapak*$this->iJumlahNenekBapak+$this->iJthNenekIbu*$this->iJumlahNenekIbu+$this->iJthNenekKakek*$this->iJumlahNenekKakek+$this->iJthSaudaraSeibu*$this->iJumlahSaudaraSeibu+$this->iJthSaudariKandung*$this->iJumlahSaudariKandung+$this->iJthSaudariSebapak*$this->iJumlahSaudariSebapak+$this->iJthWanitaMerdekakan*$this->iJumlahWanitaMerdekakan);
																array_push($this->res, "Jatah Wanita yang Memerdekakan Budak (Sisa) : " . $this->tandaPemisahTitik($this->iJthWanitaMerdekakan));
															}
														}
													}
												}
											}
										}
									}
								}
								if($this->iJumlahSaudariSeibu == 1){
									$this->iJthSaudariSeibu = round($this->iHarta/6);
									array_push($this->res, "Jatah tiap Saudari Seibu (1/6) :" . $this->tandaPemisahTitik($this->iJthSaudariSeibu));
								}
								if($this->iJumlahSaudariSeibu > 1){
									$this->iJthSaudariSeibu = round($this->iHarta/3);
									array_push($this->res, "Jatah tiap Saudari Seibu (1/3) :" . $this->tandaPemisahTitik($this->iJthSaudariSeibu/$this->iJumlahSaudariSeibu));
								}
							}
						}
					}
				}
				if ($this->iJumlahIstri > 0) {
					$this->iJthIstri	= round(($this->iHarta/4)/$this->iJumlahIstri);
					if($this->iJumlahIbu == 1){
						$this->iJthIbu		= round($this->iHarta/4)*$this->iJumlahIbu;
					}
					if($this->iJumlahIbu == 0){
						if($this->iJumlahNenekBapak == 1 && $this->iJumlahNenekIbu == 0){
							$this->iJthNenekBapak = round($this->iHarta/6)*$this->iJumlahNenekBapak;
						}
						if($this->iJumlahNenekBapak == 0 && $this->iJumlahNenekIbu == 1){
							$this->iJthNenekIbu = round($this->iHarta/6)*$this->iJumlahNenekIbu;
						}
						if($this->iJumlahNenekBapak == 1 && $this->iJumlahNenekIbu == 1){
							$this->iJthNenekBapak = round($this->iHarta/12)*$this->iJumlahNenekBapak;
							$this->iJthNenekIbu = round($this->iHarta/12)*$this->iJumlahNenekIbu;
						}
						if($this->iJumlahNenekBapak == 0 && $this->iJumlahNenekIbu == 0){
							$this->iJthNenekKakek = round($this->iHarta/6)*$this->iJumlahNenekKakek;
						}
					}
					if($this->iJumlahBapak > 0){
						$this->iJthBapak	= round($this->iHarta/2);
						$this->iSisa		= $this->iHarta - ($this->iJthIstri*$this->iJumlahIstri+$this->iJthIbu+$this->iJthBapak+$this->iJthNenekBapak*$this->iJumlahNenekBapak+$this->iJthNenekIbu*$this->iJumlahNenekIbu+$this->iJthNenekKakek*$this->iJumlahNenekKakek);
						$this->iJthBapak 	= $this->iJthBapak + $this->iSisa*$this->iJumlahBapak;
						$this->iSisa		= $this->iHarta - ($this->iJthIstri*$this->iJumlahIstri+$this->iJthIbu+$this->iJthBapak+$this->iJthNenekBapak*$this->iJumlahNenekBapak+$this->iJthNenekIbu*$this->iJumlahNenekIbu+$this->iJthNenekKakek*$this->iJumlahNenekKakek);
						if ($this->iJumlahSaudaraSeibu>0) 
							array_push($this->res, "Jatah tiap Saudara Seibu : 0 (karena dihalangi oleh Bapak)");
						if ($this->iJumlahSaudaraKandung>0) 
							array_push($this->res, "Jatah tiap Saudara Kandung : 0 (karena dihalangi oleh Bapak)");
						if ($this->iJumlahSaudariKandung>0) 
							array_push($this->res, "Jatah tiap Saudari Kandung : 0 (karena dihalangi oleh Bapak)");
						if ($this->iJumlahSaudariSeibu>0) 
							array_push($this->res, "Jatah tiap Saudari Seibu : 0 (karena dihalangi oleh Bapak)");
						if ($this->iJumlahSaudaraSebapak>0) 
							array_push($this->res, "Jatah tiap Saudara Sebapak : 0 (karena dihalangi oleh Bapak)");
						if ($this->iJumlahSaudariSebapak>0) 
							array_push($this->res, "Jatah tiap Saudari Sebapak : 0 (karena dihalangi oleh Bapak)");
						if ($this->iJumlahPutraSaudaraKandung>0) 
							array_push($this->res, "Jatah tiap Putra Saudara Kandung : 0 (karena dihalangi oleh Bapak)");
						if ($this->iJumlahPutraSaudaraSebapak>0) 
							array_push($this->res, "Jatah tiap Putra Saudara Sebapak : 0 (karena dihalangi oleh Bapak)");
						if ($this->iJumlahPamanKandung>0) 
							array_push($this->res, "Jatah tiap Paman Kandung : 0 (karena dihalangi oleh Bapak)");
						if ($this->iJumlahPamanSebapak>0) 
							array_push($this->res, "Jatah tiap Paman Sebapak : 0 (karena dihalangi oleh Bapak)");
						if ($this->iJumlahPutraPamanKandung>0) 
							array_push($this->res, "Jatah tiap Putra Paman Kandung : 0 (karena dihalangi oleh Bapak)");
						if ($this->iJumlahPutraPamanSebapak>0) 
							array_push($this->res, "Jatah tiap Putra Paman Sebapak : 0 (karena dihalangi oleh Bapak)");
						if ($this->iJumlahPriaMerdekakan>0) 
							array_push($this->res, "Jatah Pria yang Memerdekakan Budak : 0 (karena dihalangi oleh Bapak)");
						if ($this->iJumlahWanitaMerdekakan>0) 
							array_push($this->res, "Jatah Wanita yang Memerdekakan Budak : 0 (karena dihalangi oleh Bapak)");
					}
					if($this->iJumlahBapak == 0){
						if($this->iJumlahKakek == 1){
							$this->iJthKakek	= round($this->iHarta/2)*$this->iJumlahKakek;
							$this->iSisa		= $this->iHarta - ($this->iJthIstri*$this->iJumlahIstri+$this->iJthIbu+$this->iJthKakek+$this->iJthNenekBapak*$this->iJumlahNenekBapak+$this->iJthNenekIbu*$this->iJumlahNenekIbu+$this->iJthNenekKakek*$this->iJumlahNenekKakek);
							$this->iJthKakek 	= $this->iJthKakek + $this->iSisa*$this->iJumlahKakek;
							$this->iSisa		= $this->iHarta - ($this->iJthIstri*$this->iJumlahIstri+$this->iJthIbu+$this->iJthKakek+$this->iJthNenekBapak*$this->iJumlahNenekBapak+$this->iJthNenekIbu*$this->iJumlahNenekIbu+$this->iJthNenekKakek*$this->iJumlahNenekKakek);
							if ($this->iJumlahSaudaraSeibu>0) 
								array_push($this->res, "Jatah tiap Saudara Seibu : 0 (karena dihalangi oleh Kakek)");
							if ($this->iJumlahSaudaraKandung>0) 
								array_push($this->res, "Jatah tiap Saudara Kandung : 0 (karena dihalangi oleh Kakek)");
							if ($this->iJumlahSaudariKandung>0) 
								array_push($this->res, "Jatah tiap Saudari Kandung : 0 (karena dihalangi oleh Kakek)");
							if ($this->iJumlahSaudariSeibu>0) 
								array_push($this->res, "Jatah tiap Saudari Seibu : 0 (karena dihalangi oleh Kakek)");
							if ($this->iJumlahSaudaraSebapak>0) 
								array_push($this->res, "Jatah tiap Saudara Sebapak : 0 (karena dihalangi oleh Kakek)");
							if ($this->iJumlahSaudariSebapak>0) 
								array_push($this->res, "Jatah tiap Saudari Sebapak : 0 (karena dihalangi oleh Kakek)");
							if ($this->iJumlahPutraSaudaraKandung>0) 
								array_push($this->res, "Jatah tiap Putra Saudara Kandung : 0 (karena dihalangi oleh Kakek)");
							if ($this->iJumlahPutraSaudaraSebapak>0) 
								array_push($this->res, "Jatah tiap Putra Saudara Sebapak : 0 (karena dihalangi oleh Kakek)");
							if ($this->iJumlahPamanKandung>0) 
								array_push($this->res, "Jatah tiap Paman Kandung : 0 (karena dihalangi oleh Kakek)");
							if ($this->iJumlahPamanSebapak>0) 
								array_push($this->res, "Jatah tiap Paman Sebapak : 0 (karena dihalangi oleh Kakek)");
							if ($this->iJumlahPutraPamanKandung>0) 
								array_push($this->res, "Jatah tiap Putra Paman Kandung : 0 (karena dihalangi oleh Kakek)");
							if ($this->iJumlahPutraPamanSebapak>0) 
								array_push($this->res, "Jatah tiap Putra Paman Sebapak : 0 (karena dihalangi oleh Kakek)");
							if ($this->iJumlahPriaMerdekakan>0) 
								array_push($this->res, "Jatah Pria yang Memerdekakan Budak : 0 (karena dihalangi oleh Kakek)");
							if ($this->iJumlahWanitaMerdekakan>0) 
								array_push($this->res, "Jatah Wanita yang Memerdekakan Budak : 0 (karena dihalangi oleh Kakek)");
						}
						if($this->iJumlahKakek == 0){
							if($this->iJumlahSaudaraSeibu == 1){
								$this->iJthSaudaraSeibu = round($this->iHarta/6);
								$this->iSisa		= $this->iHarta - ($this->iJthIstri*$this->iJumlahIstri+$this->iJthIbu+$this->iJthNenekBapak*$this->iJumlahNenekBapak+$this->iJthNenekIbu*$this->iJumlahNenekIbu+$this->iJthNenekKakek*$this->iJumlahNenekKakek+$this->iJthSaudaraSeibu);
								array_push($this->res, "Jatah tiap Saudara Seibu (1/6) : " . $this->tandaPemisahTitik($this->iJthSaudaraSeibu));
							}
							if($this->iJumlahSaudaraSeibu > 1){
								$this->iJthSaudaraSeibu = round($this->iHarta/3);
								$this->iSisa		= $this->iHarta - ($this->iJthIstri*$this->iJumlahIstri+$this->iJthIbu+$this->iJthNenekBapak*$this->iJumlahNenekBapak+$this->iJthNenekIbu*$this->iJumlahNenekIbu+$this->iJthNenekKakek*$this->iJumlahNenekKakek+$this->iJthSaudaraSeibu);
								array_push($this->res, "Jatah tiap Saudara Seibu (1/3) : " . $this->tandaPemisahTitik($this->iJthSaudaraSeibu/$this->iJumlahSaudaraSeibu));
							}
							if($this->iJumlahSaudaraKandung > 0){
								$this->iSisa		= $this->iHarta - ($this->iJthIstri*$this->iJumlahIstri+$this->iJthIbu+$this->iJthNenekBapak*$this->iJumlahNenekBapak+$this->iJthNenekIbu*$this->iJumlahNenekIbu+$this->iJthNenekKakek*$this->iJumlahNenekKakek+$this->iJthSaudaraSeibu);
								if($this->iJumlahSaudariKandung == 0){
									$this->iJthSaudaraKandung = round($this->iSisa/$this->iJumlahSaudaraKandung);
								}
								if($this->iJumlahSaudariKandung > 0){
									$this->iJthSaudaraKandung = round($this->iSisa/($this->iJumlahSaudaraKandung + $this->iJumlahSaudariKandung));
									$this->iJthSaudariKandung = $this->iJthSaudaraKandung;
									array_push($this->res, "Jatah tiap Saudari Kandung (Sisa) : " . $this->tandaPemisahTitik($this->iJthSaudariKandung));
								}
								$this->iSisa		= $this->iHarta - ($this->iJthIstri*$this->iJumlahIstri+$this->iJthIbu+$this->iJthNenekBapak*$this->iJumlahNenekBapak+$this->iJthNenekIbu*$this->iJumlahNenekIbu+$this->iJthNenekKakek*$this->iJumlahNenekKakek+$this->iJthSaudaraSeibu+$this->iJthSaudaraKandung*$this->iJumlahSaudaraKandung+$this->iJthSaudariKandung*$this->iJumlahSaudariKandung);
								array_push($this->res, "Jatah tiap Saudara Kandung : " . $this->tandaPemisahTitik($this->iJthSaudaraKandung));
								if($this->iJumlahSaudaraSebapak > 0) array_push($this->res, "Jatah tiap Saudara Sebapak : 0 (Karena dihalangi Saudara Sekandung)");
								if($this->iJumlahSaudaraSebapak > 0) array_push($this->res, "Jatah tiap Saudara Sebapak : 0 (Karena dihalangi Saudara Sekandung)");
								if($this->iJumlahPutraSaudaraKandung > 0) array_push($this->res, "Jatah tiap Putra dari Saudara Kandung : 0 (Karena dihalangi Saudara Sekandung)");
								if($this->iJumlahPutraSaudaraSebapak > 0) array_push($this->res, "Jatah tiap Putra dari Saudara Sebapak : 0 (Karena dihalangi Saudara Sekandung)");
								if($this->iJumlahPamanKandung > 0) array_push($this->res, "Jatah tiap Paman Sekandung : 0 (Karena dihalangi Saudara Sekandung)");
								if($this->iJumlahPamanSebapak > 0) array_push($this->res, "Jatah tiap Paman Sebapak : 0 (Karena dihalangi Saudara Sekandung)");
								if($this->iJumlahPutraPamanKandung > 0) array_push($this->res, "Jatah tiap Putra dari Paman Kandung : 0 (Karena dihalangi Saudara Sekandung)");
								if($this->iJumlahPutraPamanSebapak > 0) array_push($this->res, "Jatah tiap Putra dari Paman Sebapak : 0 (Karena dihalangi Saudara Sekandung)");
								if($this->iJumlahPriaMerdekakan > 0) array_push($this->res, "Jatah tiap Pria yang Memerdekakan Budak : 0 (Karena dihalangi Saudara Sekandung)");
								if($this->iJumlahWanitaMerdekakan > 0) array_push($this->res, "Jatah tiap Wanita yang Memerdekakan Budak : 0 (Karena dihalangi Saudara Sekandung)");
								if($this->iJumlahSaudariSebapak > 0) array_push($this->res, "Jatah tiap Saudari Sebapak : 0 (Karena dihalangi Saudara Sekandung)");
							}
							if($this->iJumlahSaudaraKandung == 0){
								if($this->iJumlahSaudariKandung > 1){
									$this->iJthSaudariKandung = round((2*$this->iHarta/3)/$this->iJumlahSaudariKandung);
									$this->iSisa		= $this->iHarta - ($this->iJthIstri*$this->iJumlahIstri+$this->iJthIbu+$this->iJthNenekBapak*$this->iJumlahNenekBapak+$this->iJthNenekIbu*$this->iJumlahNenekIbu+$this->iJthNenekKakek*$this->iJumlahNenekKakek+$this->iJthSaudaraSeibu+$this->iJthSaudariKandung*$this->iJumlahSaudariKandung);
									array_push($this->res, "Jatah tiap Saudari Kandung (2/3) : " . $this->tandaPemisahTitik($this->iJthSaudariKandung));
									if($this->iJumlahSaudariSebapak > 0 && $this->iJumlahSaudaraSebapak == 0){array_push($this->res, "Jatah tiap Saudari Sebapak : 0(Karena dihalangi 2 Saudari Kandung atau lebih)");}
								}
								if($this->iJumlahSaudariKandung == 1 || $this->iJumlahSaudariKandung == 0){
									$this->iJthSaudariKandung = round($this->iHarta/2);
									if($this->iJumlahSaudaraSebapak == 0 && $this->iJumlahSaudariSebapak == 1){
										$this->iJthSaudariSebapak = round($this->iHarta/2);
										array_push($this->res, "Jatah tiap Saudari Sebapak (1/2) : " . $this->tandaPemisahTitik($this->iJthSaudariSebapak/$this->iJumlahSaudariSebapak));
									}
									if($this->iJumlahSaudaraSebapak == 0 && $this->iJumlahSaudariSebapak > 1){
										$this->iJthSaudariSebapak = round((2*$this->iHarta/3)/$this->iJumlahSaudariSebapak);
										array_push($this->res, "Jatah tiap Saudari Sebapak (2/3) : " . $this->tandaPemisahTitik($this->iJthSaudariSebapak));
									}
									$this->iSisa		= $this->iHarta - ($this->iJthIstri*$this->iJumlahIstri+$this->iJthIbu+$this->iJthNenekBapak*$this->iJumlahNenekBapak+$this->iJthNenekIbu*$this->iJumlahNenekIbu+$this->iJthNenekKakek*$this->iJumlahNenekKakek+$this->iJthSaudaraSeibu+$this->iJthSaudariKandung*$this->iJumlahSaudariKandung+$this->iJthSaudariSebapak*$this->iJumlahSaudariSebapak);
									if($this->iJumlahSaudariKandung == 1) array_push($this->res, "Jatah tiap Saudari Kandung (1/2) : " . $this->tandaPemisahTitik($this->iJthSaudariKandung/$this->iJumlahSaudariKandung));
								}
								if($this->iJumlahSaudaraSebapak > 0){
									$this->iSisa		= $this->iHarta - ($this->iJthIstri*$this->iJumlahIstri+$this->iJthIbu+$this->iJthNenekBapak*$this->iJumlahNenekBapak+$this->iJthNenekIbu*$this->iJumlahNenekIbu+$this->iJthNenekKakek*$this->iJumlahNenekKakek+$this->iJthSaudaraSeibu);
									$this->iJthSaudaraSebapak = round($this->iSisa/($this->iJumlahSaudaraSebapak+$this->iJumlahSaudariSebapak));
									$this->iJthSaudariSebapak = $this->iJthSaudaraSebapak;
									$this->iSisa		= $this->iHarta - ($this->iJthIstri*$this->iJumlahIstri+$this->iJthIbu+$this->iJthNenekBapak*$this->iJumlahNenekBapak+$this->iJthNenekIbu*$this->iJumlahNenekIbu+$this->iJthNenekKakek*$this->iJumlahNenekKakek+$this->iJthSaudaraSeibu+$this->iJthSaudaraSebapak*$this->iJumlahSaudaraSebapak+$this->iJthSaudariSebapak*$this->iJumlahSaudariSebapak);
									array_push($this->res, "Jatah tiap Saudara Sebapak (Sisa) : " . $this->tandaPemisahTitik($this->iJthSaudaraSebapak));
									if($this->iJumlahSaudariSebapak > 0) array_push($this->res, "Jatah tiap Saudari Sebapak (Sisa) : " . $this->tandaPemisahTitik($this->iJthSaudariSebapak));
									if($this->iJumlahPutraSaudaraKandung > 0) array_push($this->res, "Jatah tiap Putra dari Saudara Kandung : 0 (Karena dihalangi Saudara Sebapak)");
									if($this->iJumlahPutraSaudaraSebapak > 0) array_push($this->res, "Jatah tiap Putra dari Saudara Sebapak : 0 (Karena dihalangi Saudara Sebapak)");
									if($this->iJumlahPamanKandung > 0) array_push($this->res, "Jatah tiap Paman Sekandung : 0 (Karena dihalangi Saudara Sebapak)");
									if($this->iJumlahPamanSebapak > 0) array_push($this->res, "Jatah tiap Paman Sebapak : 0 (Karena dihalangi Saudara Sebapak)");
									if($this->iJumlahPutraPamanKandung > 0) array_push($this->res, "Jatah tiap Putra dari Paman Kandung : 0 (Karena dihalangi Saudara Sebapak)");
									if($this->iJumlahPutraPamanSebapak > 0) array_push($this->res, "Jatah tiap Putra dari Paman Sebapak : 0 (Karena dihalangi Saudara Sebapak)");
									if($this->iJumlahPriaMerdekakan > 0) array_push($this->res, "Jatah tiap Pria yang Memerdekakan Budak : 0 (Karena dihalangi Saudara Sebapak)");
									if($this->iJumlahWanitaMerdekakan > 0) array_push($this->res, "Jatah tiap Wanita yang Memerdekakan Budak : 0 (Karena dihalangi Saudara Sebapak)");
								}
								if($this->iJumlahSaudaraSebapak == 0){
									$this->iSisa		= $this->iHarta - ($this->iJthIstri*$this->iJumlahIstri+$this->iJthIbu+$this->iJthNenekBapak*$this->iJumlahNenekBapak+$this->iJthNenekIbu*$this->iJumlahNenekIbu+$this->iJthNenekKakek*$this->iJumlahNenekKakek+$this->iJthSaudaraSeibu*$this->iJumlahSaudaraSeibu+$this->iJthSaudariKandung*$this->iJumlahSaudariKandung+$this->iJthSaudariSebapak*$this->iJumlahSaudariSebapak);
									if($this->iJumlahPutraSaudaraKandung > 0){
										$this->iJthPutraSaudaraKandung = round($this->iSisa/$this->iJumlahPutraSaudaraKandung);
										$this->iSisa		= $this->iHarta - ($this->iJthIstri*$this->iJumlahIstri+$this->iJthIbu+$this->iJthNenekBapak*$this->iJumlahNenekBapak+$this->iJthNenekIbu*$this->iJumlahNenekIbu+$this->iJthNenekKakek*$this->iJumlahNenekKakek+$this->iJthSaudaraSeibu*$this->iJumlahSaudaraSeibu+$this->iJthSaudariKandung*$this->iJumlahSaudariKandung+$this->iJthSaudariSebapak*$this->iJumlahSaudariSebapak+$this->iJthPutraSaudaraKandung*$this->iJumlahPutraSaudaraKandung);
										array_push($this->res, "Jatah tiap Putra dari Saudara Sekandung (Sisa) : " . $this->tandaPemisahTitik($this->iJthPutraSaudaraKandung));
										if($this->iJumlahPutraSaudaraSebapak > 0) array_push($this->res, "Jatah tiap Putra dari Saudara Sebapak : 0 (Karena dihalangi Putra dari Saudara Sekandung)");
										if($this->iJumlahPamanKandung > 0) array_push($this->res, "Jatah tiap Paman Sekandung : 0 (Karena dihalangi Putra dari Saudara Sekandung)");
										if($this->iJumlahPamanSebapak > 0) array_push($this->res, "Jatah tiap Paman Sebapak : 0 (Karena dihalangi Putra dari Saudara Sekandung)");
										if($this->iJumlahPutraPamanKandung > 0) array_push($this->res, "Jatah tiap Putra dari Paman Kandung : 0 (Karena dihalangi Putra dari Saudara Sekandung)");
										if($this->iJumlahPutraPamanSebapak > 0) array_push($this->res, "Jatah tiap Putra dari Paman Sebapak : 0 (Karena dihalangi Putra dari Saudara Sekandung)");
										if($this->iJumlahPriaMerdekakan > 0) array_push($this->res, "Jatah tiap Pria yang Memerdekakan Budak : 0 (Karena dihalangi Putra dari Saudara Sekandung)");
										if($this->iJumlahWanitaMerdekakan > 0) array_push($this->res, "Jatah tiap Wanita yang Memerdekakan Budak : 0 (Karena dihalangi Putra dari Saudara Sekandung)");
									}
									if($this->iJumlahPutraSaudaraKandung == 0){
										$this->iJthPutraSaudaraSebapak = round($this->iSisa/$this->iJumlahPutraSaudaraSebapak);
										if($this->iJumlahPutraSaudaraSebapak > 0){
											$this->iSisa		= $this->iHarta - ($this->iJthIstri*$this->iJumlahIstri+$this->iJthIbu+$this->iJthNenekBapak*$this->iJumlahNenekBapak+$this->iJthNenekIbu*$this->iJumlahNenekIbu+$this->iJthNenekKakek*$this->iJumlahNenekKakek+$this->iJthSaudaraSeibu*$this->iJumlahSaudaraSeibu+$this->iJthSaudariKandung*$this->iJumlahSaudariKandung+$this->iJthSaudariSebapak*$this->iJumlahSaudariSebapak+$this->iJthPutraSaudaraSebapak*$this->iJumlahPutraSaudaraSebapak);
											array_push($this->res, "Jatah tiap Putra dari Saudara Sebapak (Sisa) : " . $this->tandaPemisahTitik($this->iJthPutraSaudaraSebapak));
											if($this->iJumlahPamanKandung > 0) array_push($this->res, "Jatah tiap Paman Sekandung : 0 (Karena dihalangi Putra dari Saudara Sebapak)");
											if($this->iJumlahPamanSebapak > 0) array_push($this->res, "Jatah tiap Paman Sebapak : 0 (Karena dihalangi Putra dari Saudara Sebapak)");
											if($this->iJumlahPutraPamanKandung > 0) array_push($this->res, "Jatah tiap Putra dari Paman Kandung : 0 (Karena dihalangi Putra dari Saudara Sebapak)");
											if($this->iJumlahPutraPamanSebapak > 0) array_push($this->res, "Jatah tiap Putra dari Paman Sebapak : 0 (Karena dihalangi Putra dari Saudara Sebapak)");
											if($this->iJumlahPriaMerdekakan > 0) array_push($this->res, "Jatah tiap Pria yang Memerdekakan Budak : 0 (Karena dihalangi Putra dari Saudara Sebapak)");
											if($this->iJumlahWanitaMerdekakan > 0) array_push($this->res, "Jatah tiap Wanita yang Memerdekakan Budak : 0 (Karena dihalangi Putra dari Saudara Sebapak)");
										}
										if($this->iJumlahPutraSaudaraSebapak == 0){
											$this->iJthPamanKandung = round($this->iSisa/$this->iJumlahPamanKandung);
											if($this->iJumlahPamanKandung > 0){
												$this->iSisa		= $this->iHarta - ($this->iJthIstri*$this->iJumlahIstri+$this->iJthIbu+$this->iJthNenekBapak*$this->iJumlahNenekBapak+$this->iJthNenekIbu*$this->iJumlahNenekIbu+$this->iJthNenekKakek*$this->iJumlahNenekKakek+$this->iJthSaudaraSeibu*$this->iJumlahSaudaraSeibu+$this->iJthSaudariKandung*$this->iJumlahSaudariKandung+$this->iJthSaudariSebapak*$this->iJumlahSaudariSebapak+$this->iJthPamanKandung*$this->iJumlahPamanKandung);
												array_push($this->res, "Jatah tiap Paman Sekandung (Sisa) : " . $this->tandaPemisahTitik($this->iJthPamanKandung));
												if($this->iJumlahPamanSebapak > 0) array_push($this->res, "Jatah tiap Paman Sebapak : 0 (Karena dihalangi Paman Sekandung)");
												if($this->iJumlahPutraPamanKandung > 0) array_push($this->res, "Jatah tiap Putra dari Paman Kandung : 0 (Karena dihalangi Paman Sekandung)");
												if($this->iJumlahPutraPamanSebapak > 0) array_push($this->res, "Jatah tiap Putra dari Paman Sebapak : 0 (Karena dihalangi Paman Sekandung)");
												if($this->iJumlahPriaMerdekakan > 0) array_push($this->res, "Jatah tiap Pria yang Memerdekakan Budak : 0 (Karena dihalangi Paman Sekandung)");
												if($this->iJumlahWanitaMerdekakan > 0) array_push($this->res, "Jatah tiap Wanita yang Memerdekakan Budak : 0 (Karena dihalangi Paman Sekandung)");
											}
											if($this->iJumlahPamanKandung == 0){
												$this->iJthPamanSebapak = round($this->iSisa/$this->iJumlahPamanSebapak);
												if($this->iJumlahPamanSebapak > 0){
													$this->iSisa		= $this->iHarta - ($this->iJthIstri*$this->iJumlahIstri+$this->iJthIbu+$this->iJthNenekBapak*$this->iJumlahNenekBapak+$this->iJthNenekIbu*$this->iJumlahNenekIbu+$this->iJthNenekKakek*$this->iJumlahNenekKakek+$this->iJthSaudaraSeibu*$this->iJumlahSaudaraSeibu+$this->iJthSaudariKandung*$this->iJumlahSaudariKandung+$this->iJthSaudariSebapak*$this->iJumlahSaudariSebapak+$this->iJthPamanSebapak*$this->iJumlahPamanSebapak);
													array_push($this->res, "Jatah tiap Paman Sebapak (Sisa) : " . $this->tandaPemisahTitik($this->iJthPamanSebapak));
													if($this->iJumlahPutraPamanKandung > 0) array_push($this->res, "Jatah tiap Putra dari Paman Kandung : 0 (Karena dihalangi Paman Sebapak)");
													if($this->iJumlahPutraPamanSebapak > 0) array_push($this->res, "Jatah tiap Putra dari Paman Sebapak : 0 (Karena dihalangi Paman Sebapak)");
													if($this->iJumlahPriaMerdekakan > 0) array_push($this->res, "Jatah tiap Pria yang Memerdekakan Budak : 0 (Karena dihalangi Paman Sebapak)");
													if($this->iJumlahWanitaMerdekakan > 0) array_push($this->res, "Jatah tiap Wanita yang Memerdekakan Budak : 0 (Karena dihalangi Paman Sebapak)");
												}
												if($this->iJumlahPamanSebapak == 0){
													$this->iJthPutraPamanKandung = round($this->iSisa/$this->iJumlahPutraPamanKandung);
													if($this->iJumlahPutraPamanKandung > 0){
														$this->iSisa		= $this->iHarta - ($this->iJthIstri*$this->iJumlahIstri+$this->iJthIbu+$this->iJthNenekBapak*$this->iJumlahNenekBapak+$this->iJthNenekIbu*$this->iJumlahNenekIbu+$this->iJthNenekKakek*$this->iJumlahNenekKakek+$this->iJthSaudaraSeibu*$this->iJumlahSaudaraSeibu+$this->iJthSaudariKandung*$this->iJumlahSaudariKandung+$this->iJthSaudariSebapak*$this->iJumlahSaudariSebapak+$this->iJthPutraPamanKandung*$this->iJumlahPutraPamanKandung);
														array_push($this->res, "Jatah tiap Putra dari Paman Sekandung (Sisa) : " . $this->tandaPemisahTitik($this->iJthPutraPamanKandung));
														if($this->iJumlahPutraPamanSebapak > 0) array_push($this->res, "Jatah tiap Putra dari Paman Sebapak : 0 (Karena dihalangi Putra dari Paman Sekandung)");
														if($this->iJumlahPriaMerdekakan > 0) array_push($this->res, "Jatah tiap Pria yang Memerdekakan Budak : 0 (Karena dihalangi Putra dari Paman Sekandung)");
														if($this->iJumlahWanitaMerdekakan > 0) array_push($this->res, "Jatah tiap Wanita yang Memerdekakan Budak : 0 (Karena dihalangi Putra dari Paman Sekandung)");
													}
													if($this->iJumlahPutraPamanKandung == 0){
														$this->iJthPutraPamanSebapak = round($this->iSisa/$this->iJumlahPutraPamanSebapak);
														if($this->iJumlahPutraPamanSebapak > 0){
															$this->iSisa		= $this->iHarta - ($this->iJthIstri*$this->iJumlahIstri+$this->iJthIbu+$this->iJthNenekBapak*$this->iJumlahNenekBapak+$this->iJthNenekIbu*$this->iJumlahNenekIbu+$this->iJthNenekKakek*$this->iJumlahNenekKakek+$this->iJthSaudaraSeibu*$this->iJumlahSaudaraSeibu+$this->iJthSaudariKandung*$this->iJumlahSaudariKandung+$this->iJthSaudariSebapak*$this->iJumlahSaudariSebapak+$this->iJthPutraPamanSebapak*$this->iJumlahPutraPamanSebapak);
															array_push($this->res, "Jatah tiap Putra dari Paman Sebapak (Sisa) : " . $this->tandaPemisahTitik($this->iJthPutraPamanSebapak));
															if($this->iJumlahPriaMerdekakan > 0) array_push($this->res, "Jatah Pria yang Memerdekakan Budak : 0 (Karena dihalangi Putra dari Paman Sebapak)");
															if($this->iJumlahWanitaMerdekakan > 0) array_push($this->res, "Jatah Wanita yang Memerdekakan Budak : 0 (Karena dihalangi Putra dari Paman Sebapak)");
														}
														if($this->iJumlahPutraPamanSebapak == 0){
															$this->iJthPriaMerdekakan = round($this->iSisa/($this->iJumlahPriaMerdekakan+$this->iJumlahWanitaMerdekakan));
															$this->iJthWanitaMerdekakan = $this->iJthPriaMerdekakan;
															if($this->iJumlahPriaMerdekakan > 0){
																$this->iSisa		= $this->iHarta - ($this->iJthIstri*$this->iJumlahIstri+$this->iJthIbu+$this->iJthNenekBapak*$this->iJumlahNenekBapak+$this->iJthNenekIbu*$this->iJumlahNenekIbu+$this->iJthNenekKakek*$this->iJumlahNenekKakek+$this->iJthSaudaraSeibu*$this->iJumlahSaudaraSeibu+$this->iJthSaudariKandung*$this->iJumlahSaudariKandung+$this->iJthSaudariSebapak*$this->iJumlahSaudariSebapak+$this->iJthPriaMerdekakan*$this->iJumlahPriaMerdekakan);
																array_push($this->res, "Jatah Pria yang Memerdekakan Budak (Sisa) : " . $this->tandaPemisahTitik($this->iJthPriaMerdekakan));
															}
															if($this->iJumlahWanitaMerdekakan > 0){
																$this->iSisa		= $this->iHarta - ($this->iJthIstri*$this->iJumlahIstri+$this->iJthIbu+$this->iJthNenekBapak*$this->iJumlahNenekBapak+$this->iJthNenekIbu*$this->iJumlahNenekIbu+$this->iJthNenekKakek*$this->iJumlahNenekKakek+$this->iJthSaudaraSeibu*$this->iJumlahSaudaraSeibu+$this->iJthSaudariKandung*$this->iJumlahSaudariKandung+$this->iJthSaudariSebapak*$this->iJumlahSaudariSebapak+$this->iJthWanitaMerdekakan*$this->iJumlahWanitaMerdekakan);
																array_push($this->res, "Jatah Wanita yang Memerdekakan Budak (Sisa) : " . $this->tandaPemisahTitik($this->iJthWanitaMerdekakan));
															}
														}
													}
												}
											}
										}
									}
								}
								if($this->iJumlahSaudariSeibu == 1){
									$this->iJthSaudariSeibu = round($this->iHarta/6);
									array_push($this->res, "Jatah tiap Saudari Seibu (1/6) :" . $this->tandaPemisahTitik($this->iJthSaudariSeibu));
								}
								if($this->iJumlahSaudariSeibu > 1){
									$this->iJthSaudariSeibu = round($this->iHarta/3);
									array_push($this->res, "Jatah tiap Saudari Seibu (1/3) :" . $this->tandaPemisahTitik($this->iJthSaudariSeibu/$this->iJumlahSaudariSeibu));
								}
							}
						}
					}
				}
			}
			if($this->iJumlahCucuLaki > 0 && $this->iJumlahCucuPerempuan >0){
				if ($this->iJumlahSuami == 1) {
					$this->iJthSuami = round($this->iHarta/4);
				}
				if ($this->iJumlahIstri > 0) {
					$this->iJthIstri = round($this->iHarta/8);
				}
				if ($this->iJumlahIbu == 1) {
					$this->iJthIbu = round($this->iHarta/6);
				}
				if($this->iJumlahIbu == 0){
					if($this->iJumlahNenekBapak == 1 && $this->iJumlahNenekIbu == 0){
						$this->iJthNenekBapak = round($this->iHarta/6)*$this->iJumlahNenekBapak;
					}
					if($this->iJumlahNenekBapak == 0 && $this->iJumlahNenekIbu == 1){
						$this->iJthNenekIbu = round($this->iHarta/6)*$this->iJumlahNenekIbu;
					}
					if($this->iJumlahNenekBapak == 1 && $this->iJumlahNenekIbu == 1){
						$this->iJthNenekBapak = round($this->iHarta/12)*$this->iJumlahNenekBapak;
						$this->iJthNenekIbu = round($this->iHarta/12)*$this->iJumlahNenekIbu;
					}
					if($this->iJumlahNenekBapak == 0 && $this->iJumlahNenekIbu == 0){
						$this->iJthNenekKakek = round($this->iHarta/6)*$this->iJumlahNenekKakek;
					}
				}
				if ($this->iJumlahBapak == 1) {
					$this->iJthBapak = round($this->iHarta/6);
				}
				if ($this->iJumlahBapak == 0) {
					$this->iJthKakek = round($this->iHarta/6)*$this->iJumlahKakek;
				}
				$this->iSisa	= $this->iHarta - ($this->iJthSuami*$this->iJumlahSuami + $this->iJthIstri*$this->iJumlahIstri + $this->iJthIbu*$this->iJumlahIbu + $this->iJthBapak*$this->iJumlahBapak + $this->iJthKakek*$this->iJumlahKakek+$this->iJthNenekBapak*$this->iJumlahNenekBapak+$this->iJthNenekIbu*$this->iJumlahNenekIbu+$this->iJthNenekKakek*$this->iJumlahNenekKakek);
				$this->iJthCucuLaki = round((2 * $this->iSisa)/(2*$this->iJumlahCucuLaki+$this->iJumlahCucuPerempuan));
				$this->iJthCucuPerempuan = round(($this->iSisa)/(2*$this->iJumlahCucuLaki+$this->iJumlahCucuPerempuan));
				$this->iSisa	= $this->iHarta - ($this->iJthSuami*$this->iJumlahSuami + $this->iJthIstri*$this->iJumlahIstri + $this->iJthIbu*$this->iJumlahIbu + $this->iJthBapak*$this->iJumlahBapak + $this->iJthKakek*$this->iJumlahKakek+$this->iJthNenekBapak*$this->iJumlahNenekBapak+$this->iJthNenekIbu*$this->iJumlahNenekIbu+$this->iJthNenekKakek*$this->iJumlahNenekKakek + $this->iJthCucuLaki*$this->iJumlahCucuLaki + $this->iJthCucuPerempuan*$this->iJumlahCucuPerempuan);
				array_push($this->res, "Jatah tiap Cucu Laki-laki (Sisa) : " . $this->tandaPemisahTitik($this->iJthCucuLaki));
				array_push($this->res, "Jatah tiap Cucu Perempuan (Sisa) : " . $this->tandaPemisahTitik($this->iJthCucuPerempuan));
				if ($this->iJumlahSaudaraKandung>0) 
					array_push($this->res, "Jatah tiap Saudara Kandung : 0 (karena dihalangi oleh Cucu Laki-Laki)");
				if ($this->iJumlahSaudariKandung>0) 
					array_push($this->res, "Jatah tiap Saudari kandung : 0 (karena dihalangi oleh Cucu Laki-Laki)");
				if ($this->iJumlahSaudaraSebapak>0) 
					array_push($this->res, "Jatah tiap Saudara Sebapak : 0 (karena dihalangi oleh Cucu Laki-Laki)");
				if ($this->iJumlahSaudaraSeibu>0) 
					array_push($this->res, "Jatah tiap Saudara Seibu : 0 (karena dihalangi oleh Cucu Laki-Laki)");
				if ($this->iJumlahSaudariSebapak>0) 
					array_push($this->res, "Jatah tiap Saudari Sebapak : 0 (karena dihalangi oleh Cucu Laki-Laki)");
				if ($this->iJumlahSaudariSeibu>0) 
					array_push($this->res, "Jatah tiap Saudari Seibu : 0 (karena dihalangi oleh Cucu Laki-Laki)");
				if ($this->iJumlahPutraSaudaraKandung>0) 
					array_push($this->res, "Jatah tiap Putra Saudara Kandung : 0 (karena dihalangi oleh Cucu Laki-Laki)");
				if ($this->iJumlahPutraSaudaraSebapak>0) 
					array_push($this->res, "Jatah tiap Putra Saudara Sebapak : 0 (karena dihalangi oleh Cucu Laki-Laki)");
				if ($this->iJumlahPamanKandung>0) 
					array_push($this->res, "Jatah tiap Paman Kandung : 0 (karena dihalangi oleh Cucu Laki-Laki)");
				if ($this->iJumlahPamanSebapak>0) 
					array_push($this->res, "Jatah tiap Paman Sebapak : 0 (karena dihalangi oleh Cucu Laki-Laki)");
				if ($this->iJumlahPutraPamanKandung>0) 
					array_push($this->res, "Jatah tiap Putra Paman Kandung : 0 (karena dihalangi oleh Cucu Laki-Laki)");
				if ($this->iJumlahPutraPamanSebapak>0) 
					array_push($this->res, "Jatah tiap Putra Paman Sebapak : 0 (karena dihalangi oleh Cucu Laki-Laki)");
				if ($this->iJumlahPriaMerdekakan>0) 
					array_push($this->res, "Jatah Pria yang Memerdekakan Budak : 0 (karena dihalangi oleh Cucu Laki-Laki)");
				if ($this->iJumlahWanitaMerdekakan>0) 
					array_push($this->res, "Jatah Wanita yang Memerdekakan Budak : 0 (karena dihalangi oleh Cucu Laki-Laki)");
			}
			if ($this->iJumlahCucuLaki > 0 && $this->iJumlahCucuPerempuan == 0){
				if ($this->iJumlahSuami == 1) {
					$this->iJthSuami = round($this->iHarta/4);
				}
				if ($this->iJumlahIstri > 0) {
					$this->iJthIstri = round(($this->iHarta/8)/$this->iJumlahIstri);
				}
				if ($this->iJumlahIbu == 1) {
					$this->iJthIbu = round($this->iHarta/6);
				}
				if($this->iJumlahIbu == 0){
					if($this->iJumlahNenekBapak == 1 && $this->iJumlahNenekIbu == 0){
						$this->iJthNenekBapak = round($this->iHarta/6)*$this->iJumlahNenekBapak;
					}
					if($this->iJumlahNenekBapak == 0 && $this->iJumlahNenekIbu == 1){
						$this->iJthNenekIbu = round($this->iHarta/6)*$this->iJumlahNenekIbu;
					}
					if($this->iJumlahNenekBapak == 1 && $this->iJumlahNenekIbu == 1){
						$this->iJthNenekBapak = round($this->iHarta/12)*$this->iJumlahNenekBapak;
						$this->iJthNenekIbu = round($this->iHarta/12)*$this->iJumlahNenekIbu;
					}
					if($this->iJumlahNenekBapak == 0 && $this->iJumlahNenekIbu == 0){
						$this->iJthNenekKakek = round($this->iHarta/6)*$this->iJumlahNenekKakek;
					}
				}
				if ($this->iJumlahBapak == 1) {
					$this->iJthBapak = round($this->iHarta/6);
				}
				if ($this->iJumlahBapak == 0) {
					$this->iJthKakek = round($this->iHarta/6)*$this->iJumlahKakek;
				}
				$this->iSisa = $this->iHarta - ($this->iJthSuami*$this->iJumlahSuami + $this->iJthIstri*$this->iJumlahIstri + $this->iJthIbu*$this->iJumlahIbu + $this->iJthBapak*$this->iJumlahBapak + $this->iJthKakek*$this->iJumlahKakek+$this->iJthNenekBapak*$this->iJumlahNenekBapak+$this->iJthNenekIbu*$this->iJumlahNenekIbu+$this->iJthNenekKakek*$this->iJumlahNenekKakek);
				$this->iJthCucuLaki = $this->iSisa/$this->iJumlahCucuLaki;
				$this->iSisa = $this->iHarta - ($this->iJthSuami*$this->iJumlahSuami + $this->iJthIstri*$this->iJumlahIstri + $this->iJthIbu*$this->iJumlahIbu + $this->iJthBapak*$this->iJumlahBapak + $this->iJthKakek*$this->iJumlahKakek+$this->iJthNenekBapak*$this->iJumlahNenekBapak+$this->iJthNenekIbu*$this->iJumlahNenekIbu+$this->iJthNenekKakek*$this->iJumlahNenekKakek + $this->iJthCucuLaki*$this->iJumlahCucuLaki);
				array_push($this->res, "Jatah tiap Cucu Laki-laki (Sisa) : " . $this->tandaPemisahTitik($this->iJthCucuLaki));
				if ($this->iJumlahSaudaraKandung>0) 
					array_push($this->res, "Jatah tiap Saudara Kandung : 0 (karena dihalangi oleh Cucu Laki-Laki)");
				if ($this->iJumlahSaudariKandung>0) 
					array_push($this->res, "Jatah tiap Saudari kandung : 0 (karena dihalangi oleh Cucu Laki-Laki)");
				if ($this->iJumlahSaudaraSebapak>0) 
					array_push($this->res, "Jatah tiap Saudara Sebapak : 0 (karena dihalangi oleh Cucu Laki-Laki)");
				if ($this->iJumlahSaudaraSeibu>0) 
					array_push($this->res, "Jatah tiap Saudara Seibu : 0 (karena dihalangi oleh Cucu Laki-Laki)");
				if ($this->iJumlahSaudariSebapak>0) 
					array_push($this->res, "Jatah tiap Saudari Sebapak : 0 (karena dihalangi oleh Cucu Laki-Laki)");
				if ($this->iJumlahSaudariSeibu>0) 
					array_push($this->res, "Jatah tiap Saudari Seibu : 0 (karena dihalangi oleh Cucu Laki-Laki)");
				if ($this->iJumlahPutraSaudaraKandung>0) 
					array_push($this->res, "Jatah tiap Putra Saudara Kandung : 0 (karena dihalangi oleh Cucu Laki-Laki)");
				if ($this->iJumlahPutraSaudaraSebapak>0) 
					array_push($this->res, "Jatah tiap Putra Saudara Sebapak : 0 (karena dihalangi oleh Cucu Laki-Laki)");
				if ($this->iJumlahPamanKandung>0) 
					array_push($this->res, "Jatah tiap Paman Kandung : 0 (karena dihalangi oleh Cucu Laki-Laki)");
				if ($this->iJumlahPamanSebapak>0) 
					array_push($this->res, "Jatah tiap Paman Sebapak : 0 (karena dihalangi oleh Cucu Laki-Laki)");
				if ($this->iJumlahPutraPamanKandung>0) 
					array_push($this->res, "Jatah tiap Putra Paman Kandung : 0 (karena dihalangi oleh Cucu Laki-Laki)");
				if ($this->iJumlahPutraPamanSebapak>0) 
					array_push($this->res, "Jatah tiap Putra Paman Sebapak : 0 (karena dihalangi oleh Cucu Laki-Laki)");
				if ($this->iJumlahPriaMerdekakan>0) 
					array_push($this->res, "Jatah Pria yang Memerdekakan Budak : 0 (karena dihalangi oleh Cucu Laki-Laki)");
				if ($this->iJumlahWanitaMerdekakan>0) 
					array_push($this->res, "Jatah Wanita yang Memerdekakan Budak : 0 (karena dihalangi oleh Cucu Laki-Laki)");
			}
			if ($this->iJumlahCucuLaki == 0 && $this->iJumlahCucuPerempuan > 0){
				if ($this->iJumlahSuami == 1) {
					$this->iJthSuami = round($this->iHarta/4);
				}
				if ($this->iJumlahIstri > 0) {
					$this->iJthIstri = round(($this->iHarta/8)/$this->iJumlahIstri);
				}
				if ($this->iJumlahIbu == 1) {
					$this->iJthIbu = round($this->iHarta/6);
				}
				if($this->iJumlahIbu == 0){
					if($this->iJumlahNenekBapak == 1 && $this->iJumlahNenekIbu == 0){
						$this->iJthNenekBapak = round($this->iHarta/6)*$this->iJumlahNenekBapak;
					}
					if($this->iJumlahNenekBapak == 0 && $this->iJumlahNenekIbu == 1){
						$this->iJthNenekIbu = round($this->iHarta/6)*$this->iJumlahNenekIbu;
					}
					if($this->iJumlahNenekBapak == 1 && $this->iJumlahNenekIbu == 1){
						$this->iJthNenekBapak = round($this->iHarta/12)*$this->iJumlahNenekBapak;
						$this->iJthNenekIbu = round($this->iHarta/12)*$this->iJumlahNenekIbu;
					}
					if($this->iJumlahNenekBapak == 0 && $this->iJumlahNenekIbu == 0){
						$this->iJthNenekKakek = round($this->iHarta/6)*$this->iJumlahNenekKakek;
					}
				}
				if ($this->iJumlahBapak == 1) {
					$this->iJthBapak = round($this->iHarta/6);
					if ($this->iJumlahSaudaraSeibu>0) 
						array_push($this->res, "Jatah tiap Saudara Seibu : 0 (karena dihalangi oleh Bapak)");
					if ($this->iJumlahSaudaraKandung>0) 
						array_push($this->res, "Jatah tiap Saudara Kandung : 0 (karena dihalangi oleh Bapak)");
					if ($this->iJumlahSaudariKandung>0) 
						array_push($this->res, "Jatah tiap Saudari Kandung : 0 (karena dihalangi oleh Bapak)");
					if ($this->iJumlahSaudariSeibu>0) 
						array_push($this->res, "Jatah tiap Saudari Seibu : 0 (karena dihalangi oleh Bapak)");
					if ($this->iJumlahSaudaraSebapak>0) 
						array_push($this->res, "Jatah tiap Saudara Sebapak : 0 (karena dihalangi oleh Bapak)");
					if ($this->iJumlahSaudariSebapak>0) 
						array_push($this->res, "Jatah tiap Saudari Sebapak : 0 (karena dihalangi oleh Bapak)");
					if ($this->iJumlahPutraSaudaraKandung>0) 
						array_push($this->res, "Jatah tiap Putra Saudara Kandung : 0 (karena dihalangi oleh Bapak)");
					if ($this->iJumlahPutraSaudaraSebapak>0) 
						array_push($this->res, "Jatah tiap Putra Saudara Sebapak : 0 (karena dihalangi oleh Bapak)");
					if ($this->iJumlahPamanKandung>0) 
						array_push($this->res, "Jatah tiap Paman Kandung : 0 (karena dihalangi oleh Bapak)");
					if ($this->iJumlahPamanSebapak>0) 
						array_push($this->res, "Jatah tiap Paman Sebapak : 0 (karena dihalangi oleh Bapak)");
					if ($this->iJumlahPutraPamanKandung>0) 
						array_push($this->res, "Jatah tiap Putra Paman Kandung : 0 (karena dihalangi oleh Bapak)");
					if ($this->iJumlahPutraPamanSebapak>0) 
						array_push($this->res, "Jatah tiap Putra Paman Sebapak : 0 (karena dihalangi oleh Bapak)");
					if ($this->iJumlahPriaMerdekakan>0) 
						array_push($this->res, "Jatah Pria yang Memerdekakan Budak : 0 (karena dihalangi oleh Bapak)");
					if ($this->iJumlahWanitaMerdekakan>0) 
						array_push($this->res, "Jatah Wanita yang Memerdekakan Budak : 0 (karena dihalangi oleh Bapak)");
				}
				if ($this->iJumlahBapak == 0) {
					$this->iJthKakek = round($this->iHarta/6)*$this->iJumlahKakek;
				}
				if ($this->iJumlahCucuPerempuan == 1){
					$this->iJthCucuPerempuan = round($this->iHarta/2);
					array_push($this->res, "Jatah tiap Cucu Perempuan (1/2) : " . $this->tandaPemisahTitik($this->iJthCucuPerempuan));
				}
				else{
					$this->iJthCucuPerempuan = round((2 * $this->iHarta)/(3 * $this->iJumlahCucuPerempuan));
					array_push($this->res, "Jatah tiap Cucu Perempuan (2/3) : " . $this->tandaPemisahTitik($this->iJthCucuPerempuan));
				}
				$this->iSisa	= $this->iHarta - ($this->iJthSuami*$this->iJumlahSuami + $this->iJthIstri*$this->iJumlahIstri + $this->iJthIbu*$this->iJumlahIbu + $this->iJthBapak*$this->iJumlahBapak + $this->iJthKakek*$this->iJumlahKakek+$this->iJthNenekBapak*$this->iJumlahNenekBapak+$this->iJthNenekIbu*$this->iJumlahNenekIbu+$this->iJthNenekKakek*$this->iJumlahNenekKakek + ($this->iJthCucuPerempuan * $this->iJumlahCucuPerempuan));
				if ($this->iJumlahBapak == 1) {
					$this->iJthBapak = round($this->iHarta/6);
					$this->iJthBapak = $this->iJthBapak + $this->iSisa*$this->iJumlahBapak;
				}
				if ($this->iJumlahBapak == 0) {
					if($this->iJumlahKakek == 1){
						$this->iJthKakek = round($this->iHarta/6);
						$this->iJthKakek = $this->iJthKakek + $this->iSisa*$this->iJumlahKakek;
						if ($this->iJumlahSaudaraSeibu>0) 
							array_push($this->res, "Jatah tiap Saudara Seibu : 0 (karena dihalangi oleh Kakek)");
						if ($this->iJumlahSaudaraKandung>0) 
							array_push($this->res, "Jatah tiap Saudara Kandung : 0 (karena dihalangi oleh Kakek)");
						if ($this->iJumlahSaudariKandung>0) 
							array_push($this->res, "Jatah tiap Saudari Kandung : 0 (karena dihalangi oleh Kakek)");
						if ($this->iJumlahSaudariSeibu>0) 
							array_push($this->res, "Jatah tiap Saudari Seibu : 0 (karena dihalangi oleh Kakek)");
						if ($this->iJumlahSaudaraSebapak>0) 
							array_push($this->res, "Jatah tiap Saudara Sebapak : 0 (karena dihalangi oleh Kakek)");
						if ($this->iJumlahSaudariSebapak>0) 
							array_push($this->res, "Jatah tiap Saudari Sebapak : 0 (karena dihalangi oleh Kakek)");
						if ($this->iJumlahPutraSaudaraKandung>0) 
							array_push($this->res, "Jatah tiap Putra Saudara Kandung : 0 (karena dihalangi oleh Kakek)");
						if ($this->iJumlahPutraSaudaraSebapak>0) 
							array_push($this->res, "Jatah tiap Putra Saudara Sebapak : 0 (karena dihalangi oleh Kakek)");
						if ($this->iJumlahPamanKandung>0) 
							array_push($this->res, "Jatah tiap Paman Kandung : 0 (karena dihalangi oleh Kakek)");
						if ($this->iJumlahPamanSebapak>0) 
							array_push($this->res, "Jatah tiap Paman Sebapak : 0 (karena dihalangi oleh Kakek)");
						if ($this->iJumlahPutraPamanKandung>0) 
							array_push($this->res, "Jatah tiap Putra Paman Kandung : 0 (karena dihalangi oleh Kakek)");
						if ($this->iJumlahPutraPamanSebapak>0) 
							array_push($this->res, "Jatah tiap Putra Paman Sebapak : 0 (karena dihalangi oleh Kakek)");
						if ($this->iJumlahPriaMerdekakan>0) 
							array_push($this->res, "Jatah Pria yang Memerdekakan Budak : 0 (karena dihalangi oleh Kakek)");
						if ($this->iJumlahWanitaMerdekakan>0) 
							array_push($this->res, "Jatah Wanita yang Memerdekakan Budak : 0 (karena dihalangi oleh Kakek)");
					}
					if($this->iJumlahKakek == 0){
							if($this->iJumlahSaudaraKandung > 0){
								$this->iSisa		= $this->iHarta - ($this->iJthSuami+$this->iJthIbu+$this->iJthCucuPerempuan+$this->iJthNenekBapak*$this->iJumlahNenekBapak+$this->iJthNenekIbu*$this->iJumlahNenekIbu+$this->iJthNenekKakek*$this->iJumlahNenekKakek+$this->iJthSaudaraSeibu);
								if($this->iJumlahSaudariKandung == 0){
									$this->iJthSaudaraKandung = round($this->iSisa/$this->iJumlahSaudaraKandung);
								}
								if($this->iJumlahSaudariKandung > 0){
									$this->iJthSaudaraKandung = round($this->iSisa/($this->iJumlahSaudaraKandung + $this->iJumlahSaudariKandung));
									$this->iJthSaudariKandung = $this->iJthSaudaraKandung;
									array_push($this->res, "Jatah tiap Saudari Kandung (Sisa) : " . $this->tandaPemisahTitik($this->iJthSaudariKandung));
									if($this->iJumlahPutraSaudaraKandung > 0) array_push($this->res, "Jatah tiap Putra dari Saudara Kandung : 0 (Karena dihalangi Saudari Sekandung)");
									if($this->iJumlahPutraSaudaraSebapak > 0) array_push($this->res, "Jatah tiap Putra dari Saudara Sebapak : 0 (Karena dihalangi Saudari Sekandung)");
									if($this->iJumlahPamanKandung > 0) array_push($this->res, "Jatah tiap Paman Sekandung : 0 (Karena dihalangi Saudari Sekandung)");
									if($this->iJumlahPamanSebapak > 0) array_push($this->res, "Jatah tiap Paman Sebapak : 0 (Karena dihalangi Saudari Sekandung)");
									if($this->iJumlahPutraPamanKandung > 0) array_push($this->res, "Jatah tiap Putra dari Paman Kandung : 0 (Karena dihalangi Saudari Sekandung)");
									if($this->iJumlahPutraPamanSebapak > 0) array_push($this->res, "Jatah tiap Putra dari Paman Sebapak : 0 (Karena dihalangi Saudari Sekandung)");
									if($this->iJumlahPriaMerdekakan > 0) array_push($this->res, "Jatah tiap Pria yang Memerdekakan Budak : 0 (Karena dihalangi Saudari Sekandung)");
									if($this->iJumlahWanitaMerdekakan > 0) array_push($this->res, "Jatah tiap Wanita yang Memerdekakan Budak : 0 (Karena dihalangi Saudari Sekandung)");
								}
								$this->iSisa		= $this->iHarta - ($this->iJthSuami+$this->iJthIbu+$this->iJthNenekBapak*$this->iJumlahNenekBapak+$this->iJthNenekIbu*$this->iJumlahNenekIbu+$this->iJthNenekKakek*$this->iJumlahNenekKakek+$this->iJthSaudaraSeibu+$this->iJthSaudaraKandung*$this->iJumlahSaudaraKandung+$this->iJthSaudariKandung*$this->iJumlahSaudariKandung);
								array_push($this->res, "Jatah tiap Saudara Kandung (Sisa) : " . $this->tandaPemisahTitik($this->iJthSaudaraKandung));
								if($this->iJumlahSaudaraSebapak > 0) array_push($this->res, "Jatah tiap Saudara Sebapak : 0 (Karena dihalangi Saudara Sekandung)");
								if($this->iJumlahPutraSaudaraKandung > 0) array_push($this->res, "Jatah tiap Putra dari Saudara Kandung : 0 (Karena dihalangi Saudara Sekandung)");
								if($this->iJumlahPutraSaudaraSebapak > 0) array_push($this->res, "Jatah tiap Putra dari Saudara Sebapak : 0 (Karena dihalangi Saudara Sekandung)");
								if($this->iJumlahPamanKandung > 0) array_push($this->res, "Jatah tiap Paman Sekandung : 0 (Karena dihalangi Saudara Sekandung)");
								if($this->iJumlahPamanSebapak > 0) array_push($this->res, "Jatah tiap Paman Sebapak : 0 (Karena dihalangi Saudara Sekandung)");
								if($this->iJumlahPutraPamanKandung > 0) array_push($this->res, "Jatah tiap Putra dari Paman Kandung : 0 (Karena dihalangi Saudara Sekandung)");
								if($this->iJumlahPutraPamanSebapak > 0) array_push($this->res, "Jatah tiap Putra dari Paman Sebapak : 0 (Karena dihalangi Saudara Sekandung)");
								if($this->iJumlahPriaMerdekakan > 0) array_push($this->res, "Jatah tiap Pria yang Memerdekakan Budak : 0 (Karena dihalangi Saudara Sekandung)");
								if($this->iJumlahWanitaMerdekakan > 0) array_push($this->res, "Jatah tiap Wanita yang Memerdekakan Budak : 0 (Karena dihalangi Saudara Sekandung)");
								if($this->iJumlahSaudariSebapak > 0) array_push($this->res, "Jatah tiap Saudari Sebapak : 0 (Karena dihalangi Saudara Sekandung)");
							}
							if($this->iJumlahSaudaraKandung == 0){
								if($this->iJumlahSaudariKandung > 0 && $this->iJumlahCucuPerempuan > 0){
									$this->iSisa		= $this->iHarta - ($this->iJthSuami+$this->iJthIbu+$this->iJthCucuPerempuan+$this->iJthNenekBapak*$this->iJumlahNenekBapak+$this->iJthNenekIbu*$this->iJumlahNenekIbu+$this->iJthNenekKakek*$this->iJumlahNenekKakek+$this->iJthSaudaraSeibu);
									$this->iJthSaudariKandung = round($this->iSisa/$this->iJumlahSaudariKandung);
									$this->iSisa		= $this->iHarta - ($this->iJthSuami+$this->iJthIbu+$this->iJthCucuPerempuan+$this->iJthNenekBapak*$this->iJumlahNenekBapak+$this->iJthNenekIbu*$this->iJumlahNenekIbu+$this->iJthNenekKakek*$this->iJumlahNenekKakek+$this->iJthSaudaraSeibu+$this->iJthSaudariKandung*$this->iJumlahSaudariKandung);
									array_push($this->res, "Jatah tiap Saudari Kandung (Sisa) : " . $this->tandaPemisahTitik($this->iJthSaudariKandung/$this->iJumlahSaudariKandung));
									if($this->iJumlahSaudariSebapak > 0){array_push($this->res, "Jatah tiap Saudari Sebapak : 0(Karena dihalangi 2 Saudari Kandung atau lebih)");}
									if($this->iJumlahPutraSaudaraKandung > 0) array_push($this->res, "Jatah tiap Putra dari Saudara Kandung : 0 (Karena dihalangi Saudari Sekandung)");
									if($this->iJumlahPutraSaudaraSebapak > 0) array_push($this->res, "Jatah tiap Putra dari Saudara Sebapak : 0 (Karena dihalangi Saudari Sekandung)");
									if($this->iJumlahPamanKandung > 0) array_push($this->res, "Jatah tiap Paman Sekandung : 0 (Karena dihalangi Saudari Sekandung)");
									if($this->iJumlahPamanSebapak > 0) array_push($this->res, "Jatah tiap Paman Sebapak : 0 (Karena dihalangi Saudari Sekandung)");
									if($this->iJumlahPutraPamanKandung > 0) array_push($this->res, "Jatah tiap Putra dari Paman Kandung : 0 (Karena dihalangi Saudari Sekandung)");
									if($this->iJumlahPutraPamanSebapak > 0) array_push($this->res, "Jatah tiap Putra dari Paman Sebapak : 0 (Karena dihalangi Saudari Sekandung)");
									if($this->iJumlahPriaMerdekakan > 0) array_push($this->res, "Jatah tiap Pria yang Memerdekakan Budak : 0 (Karena dihalangi Saudari Sekandung)");
									if($this->iJumlahWanitaMerdekakan > 0) array_push($this->res, "Jatah tiap Wanita yang Memerdekakan Budak : 0 (Karena dihalangi Saudari Sekandung)");
								}
								if($this->iJumlahSaudaraSebapak > 0){
									$this->iSisa		= $this->iHarta - ($this->iJthSuami+$this->iJthIbu+$this->iJthCucuPerempuan+$this->iJthNenekBapak*$this->iJumlahNenekBapak+$this->iJthNenekIbu*$this->iJumlahNenekIbu+$this->iJthNenekKakek*$this->iJumlahNenekKakek+$this->iJthSaudaraSeibu);
									$this->iJthSaudaraSebapak = round($this->iSisa/$this->iJumlahSaudaraSebapak);
									$this->iSisa		= $this->iHarta - ($this->iJthSuami+$this->iJthIbu+$this->iJthCucuPerempuan+$this->iJthNenekBapak*$this->iJumlahNenekBapak+$this->iJthNenekIbu*$this->iJumlahNenekIbu+$this->iJthNenekKakek*$this->iJumlahNenekKakek+$this->iJthSaudaraSeibu+$this->iJthSaudaraSebapak*$this->iJumlahSaudaraSebapak);
									array_push($this->res, "Jatah tiap Saudara Sebapak (Sisa) : " . $this->tandaPemisahTitik($this->iJthSaudaraSebapak));
									if($this->iJumlahPutraSaudaraKandung > 0) array_push($this->res, "Jatah tiap Putra dari Saudara Kandung : 0 (Karena dihalangi Saudara Sebapak)");
									if($this->iJumlahPutraSaudaraSebapak > 0) array_push($this->res, "Jatah tiap Putra dari Saudara Sebapak : 0 (Karena dihalangi Saudara Sebapak)");
									if($this->iJumlahPamanKandung > 0) array_push($this->res, "Jatah tiap Paman Sekandung : 0 (Karena dihalangi Saudara Sebapak)");
									if($this->iJumlahPamanSebapak > 0) array_push($this->res, "Jatah tiap Paman Sebapak : 0 (Karena dihalangi Saudara Sebapak)");
									if($this->iJumlahPutraPamanKandung > 0) array_push($this->res, "Jatah tiap Putra dari Paman Kandung : 0 (Karena dihalangi Saudara Sebapak)");
									if($this->iJumlahPutraPamanSebapak > 0) array_push($this->res, "Jatah tiap Putra dari Paman Sebapak : 0 (Karena dihalangi Saudara Sebapak)");
									if($this->iJumlahPriaMerdekakan > 0) array_push($this->res, "Jatah tiap Pria yang Memerdekakan Budak : 0 (Karena dihalangi Saudara Sebapak)");
									if($this->iJumlahWanitaMerdekakan > 0) array_push($this->res, "Jatah tiap Wanita yang Memerdekakan Budak : 0 (Karena dihalangi Saudara Sebapak)");
								}
								if($this->iJumlahSaudariSebapak > 0  && $this->iJumlahSaudariKandung == 0){
									$this->iSisa		= $this->iHarta - ($this->iJthSuami+$this->iJthIbu+$this->iJthCucuPerempuan+$this->iJthNenekBapak*$this->iJumlahNenekBapak+$this->iJthNenekIbu*$this->iJumlahNenekIbu+$this->iJthNenekKakek*$this->iJumlahNenekKakek+$this->iJthSaudaraSeibu+$this->iJthSaudariKandung*$this->iJumlahSaudariKandung+$this->iJthSaudaraSebapak*$this->iJumlahSaudaraSebapak);
									$this->iJthSaudariSebapak = round($this->iSisa/$this->iJumlahSaudariSebapak);
									$this->iSisa		= $this->iHarta - ($this->iJthSuami+$this->iJthIbu+$this->iJthCucuPerempuan+$this->iJthNenekBapak*$this->iJumlahNenekBapak+$this->iJthNenekIbu*$this->iJumlahNenekIbu+$this->iJthNenekKakek*$this->iJumlahNenekKakek+$this->iJthSaudaraSeibu+$this->iJthSaudariKandung*$this->iJumlahSaudariKandung+$this->iJthSaudaraSebapak*$this->iJumlahSaudaraSebapak+$this->iJthSaudariSebapak*$this->iJumlahSaudariSebapak);
									array_push($this->res, "Jatah tiap Saudari Sebapak (Sisa) : " . $this->tandaPemisahTitik($this->iJthSaudariSebapak/$this->iJumlahSaudariSebapak));
									if($this->iJumlahPutraSaudaraKandung > 0) array_push($this->res, "Jatah tiap Putra dari Saudara Kandung : 0 (Karena dihalangi Saudari Sebapak)");
									if($this->iJumlahPutraSaudaraSebapak > 0) array_push($this->res, "Jatah tiap Putra dari Saudara Sebapak : 0 (Karena dihalangi Saudari Sebapak)");
									if($this->iJumlahPamanKandung > 0) array_push($this->res, "Jatah tiap Paman Sekandung : 0 (Karena dihalangi Saudari Sebapak)");
									if($this->iJumlahPamanSebapak > 0) array_push($this->res, "Jatah tiap Paman Sebapak : 0 (Karena dihalangi Saudari Sebapak)");
									if($this->iJumlahPutraPamanKandung > 0) array_push($this->res, "Jatah tiap Putra dari Paman Kandung : 0 (Karena dihalangi Saudari Sebapak)");
									if($this->iJumlahPutraPamanSebapak > 0) array_push($this->res, "Jatah tiap Putra dari Paman Sebapak : 0 (Karena dihalangi Saudari Sebapak)");
									if($this->iJumlahPriaMerdekakan > 0) array_push($this->res, "Jatah tiap Pria yang Memerdekakan Budak : 0 (Karena dihalangi Saudari Sebapak)");
									if($this->iJumlahWanitaMerdekakan > 0) array_push($this->res, "Jatah tiap Wanita yang Memerdekakan Budak : 0 (Karena dihalangi Saudari Sebapak)");
								}
							}
						}
					}
					$this->iSisa		= $this->iHarta - ($this->iJthSuami+$this->iJthIbu+$this->iJthBapak+$this->iJthKakek+$this->iJthCucuPerempuan+$this->iJthNenekBapak*$this->iJumlahNenekBapak+$this->iJthNenekIbu*$this->iJumlahNenekIbu+$this->iJthNenekKakek*$this->iJumlahNenekKakek+$this->iJthSaudaraSeibu+$this->iJthSaudaraKandung*$this->iJumlahSaudaraKandung+$this->iJthSaudariKandung+$this->iJthSaudaraSebapak*$this->iJumlahSaudaraSebapak);
				if ($this->iJumlahSaudaraSeibu>0) 
					array_push($this->res, "Jatah tiap Saudara Seibu : 0 (karena dihalangi oleh Cucu Perempuan)");
				if ($this->iJumlahSaudariSeibu>0) 
				array_push($this->res, "Jatah tiap Saudari Seibu : 0 (karena dihalangi oleh Cucu Perempuan)");
			}
		} // fake

		return $this->res;
	}

	public function getRes()
	{
		return $this->res;
	}

	public function getGagal()
	{
		return $this->gagal;
	}
}


$isian = [
		'harta' => 3000000,
		'hak1' => 0,
		'hak2' => 0,
		'hak3' => 0,
		'hak4' => 0,
		'bapak' => 1,
		'ibu' => 1,
		'suami' => 0,
		'istri' => 3,
		'putra' => 0,
		'putri' => 0,
		'cucuLk' => 0,
		'cucuPr' => 0,
		'kakek' => 1,
		'nenekB' => 0,
		'nenekI' => 0,
		'nenekK' => 0,
		'saudaraK' => 0,
		'saudariK' => 0,
		'saudaraB' => 0,
		'saudaraI' => 0,
		'saudariB' => 0,
		'saudariI' => 0,
		'putraSK' => 0,
		'putraSB' => 0,
		'pamanK' => 0,
		'pamanB' => 0,
		'putraPK' => 0,
		'putraPB' => 0,
		'pria' => 0,
		'wanita' => 0,
	];

$tes = new Waris($isian);
$tes->hitung();

if ($tes->getCek()) {
	echo "oke";
} else {
	echo "not oke";
}

echo "<br>hasil:";
echo "<pre>";
var_dump($tes->getRes());
echo "</pre>gagal:<pre>";
var_dump($tes->getGagal());
 ?>